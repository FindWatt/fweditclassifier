import os, re, sys
import setuptools
from setuptools.command.install import install

module_path = os.path.join(os.path.dirname(__file__), 'FwEditClassifier/__init__.py')
version_line = [line for line in open(module_path)
                if line.startswith('__version__')][0]

__version__ = re.findall("\d+\.\d+\.\d+", version_line)[0]


class CustomInstall(install):
    """
    @description: Installs external FindWatt dependencies before installing
    the current package.
    """
    def run(self):
        self.install_fw_dependencies()
        install.run(self)

    def install_fw_dependencies(self):
        if sys.version_info.major == 2:
            os.system("pip install git+https://bitbucket.org/FindWatt/fwtext")
        elif sys.version_info.major == 3:
            os.system("pip3 install git+https://bitbucket.org/FindWatt/fwtext")


setuptools.setup(
    name="FwEditClassifier",
    version=__version__,
    url="https://bitbucket.org/FindWatt/fweditclassifier",

    author="FindWatt",

    description="Edit Classifier for figuring out if 2 words are typos, abbreviations, or different words",
    long_description=open('README.md').read(),

    packages=setuptools.find_packages(),
    include_package_data=True,
    package_data={'': ["*.pyx"]},
    py_modules=['FwEditClassifier'],
    zip_safe=False,
    platforms='any',

    install_requires=[
        "cython",
#        "pyximport",
    ],
    cmdclass={'install': CustomInstall}
)
