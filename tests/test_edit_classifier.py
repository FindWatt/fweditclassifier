﻿from __future__ import absolute_import
import pyximport; pyximport.install()
from FwEditClassifier import *
from FwEditClassifier.edit_measurement import *
from FwEditClassifier.edit_classifier import *


class TestHelpers():
    def test_typo_changes(self):
        assert is_typo_changed(('a', 's'))
        assert is_typo_changed(('A', 'S'))
        assert not is_typo_changed(('a', 'S'))
        assert not is_typo_changed(('A', 's'))
    
    def test_singular(self):
        assert is_sing_plur('tree', 'trees')
    
    def test_plural(self):
        assert is_sing_plur('bodies', 'body')
        
    def test_tense(self):
        assert is_tense("walk", "walked")
        assert is_tense("plead", "pleading")

class TestEdits():
    def test_blank(self):
        result = edits("", "")
        assert result.added == []
        assert result.removed == []
        assert result.changed == []
        assert result.transposed == []
        
    def test_blank_word1(self):
        result = edits("", "test")
        assert result.removed == ['t', 'e', 's', 't']
        
    def test_blank_word2(self):
        result = edits("test", "")
        assert result.added == ['t', 'e', 's', 't']
        
        
    def test_added(self):
        result = edits("testa", "test")
        assert result.added == ['a']
        
    def test_addedtwo(self):
        result = edits("Company", "mpany")
        assert result.added == ['c', 'o']
        
    def test_removed(self):
        result = edits("test", "testa")
        assert result.removed == ['a']
        
    def test_removedtwo(self):
        result = edits("mpany", "Company")
        assert result.removed == ['c', 'o']
        
    def test_changed(self):
        result = edits("tast", "test")
        assert result.changed == [('a', 'e')]
        
        result = edits("tes,t", "t-sat", ignore_punctuation=False)
        assert result.changed == [('e', '-'), (',', 'a')]
        
        result = edits("test", "t-sat", ignore_punctuation=True)
        assert result.added == ['e']
        assert result.removed == ['a']
        
        result = edits("tes-t", "t-sat", ignore_punctuation=True)
        assert result.added == ['e']
        assert result.removed == ['a']
        
    def transposition(self):
        result = edits("test", "tset")
        assert result.transposed == [('s', 'e')]
        
        result = edits("test", "etst")
        assert result.transposed == [('e', 't')]
        
        result = edits("test", "etst")
        assert result.transposed == [('t', 's')]
        
    def duplication(self):
        result = edits("teestt", "test", duplication=True)
        assert result.duplicated == ['e', 't']
        result = edits("teesttt", "testt", duplication=True)
        assert result.duplicated == ['e', 't']
        
    def deduplication(self):
        result = edits("test", "teestt", duplication=True)
        assert result.deduplication == ['e', 't']
        result = edits("test", "testtt", duplication=True)
        assert result.deduplication == ['t']
        

class TestDistance():
    def test_added(self):
        assert distance("testa", "test") == 1
        assert distance("atest", "test") == 1
        
    def test_removed(self):
        assert distance("test", "testa") == 1
        assert distance("test", "atest") == 1
        
    def test_changed(self):
        assert distance("test", "tast") == 1
        assert distance("test", "fest") == 1
        assert distance("test", "tesh") == 1
        
    def test_transposed(self):
        assert distance("test", "tset") == 1
        assert distance("test", "tets") == 1
        assert distance("test", "etst") == 1
        assert distance("test", "etts") == 2
        

class TestOnlyPunctuationDifferences():
    def test_true(self):
        tn_edits = edits(".", "")
        assert only_punctuation_differences(tn_edits)
        tn_edits = edits("", ".")
        assert only_punctuation_differences(tn_edits)
        tn_edits = edits("t.e.s.t.", "test")
        assert only_punctuation_differences(tn_edits)
        tn_edits = edits("test", "t.e.s.t.")
        assert only_punctuation_differences(tn_edits)
        
    def test_false(self):
        tn_edits = edits("dest", "test")
        assert not only_punctuation_differences(tn_edits)
        tn_edits = edits("test", "dest")
        assert not only_punctuation_differences(tn_edits)
        tn_edits = edits("d.e.s.t.", "test")
        assert not only_punctuation_differences(tn_edits)
        tn_edits = edits("test", "d.e.s.t.")
        assert not only_punctuation_differences(tn_edits)
    


class TestTruncation():
    def test_truncate_blank(self):
        result = truncate("")
        assert result is None
        
    def test_truncate(self):
        result = truncate("communication")
        assert result == ("comm", "cmm")
        
        result = truncate("captain")
        assert result == ("capt", "cpt")
        
        result = truncate("black")
        assert result is None
        
        result = truncate("white")
        assert result == ("whit", "wht")
        
    def test_probability_blank(self):
        result = truncation_probability("", "captain")
        assert result == 0.0
    
    def test_probability(self):
        result = truncation_probability("capt", "captain")
        assert result == 1.0
        
        result = truncation_probability("capta", "captain")
        assert round(result, 2) == 0.80
        
        result = truncation_probability("captai", "captain")
        assert round(result, 2) == 0.33
        
        result = truncation_probability("commuincat", "communication")
        assert round(result, 2) == 0.0
        
        result = truncation_probability("commuin", "communication")
        assert round(result, 2) == 0.0
        
        result = truncation_probability("captm", "captain")
        assert round(result, 2) == 0.00
        
        result = truncation_probability("apt", "captain")
        assert round(result, 2) == 0.0
        
        result = truncation_probability("communi", "communication")
        assert round(result, 2) == 0.0


class TestAbbreviation():
    def test_removed_from_pair(self):
        assert removed_from_pair("c", "blak", "black")
        assert removed_from_pair("k", "blac", "black")
        assert not removed_from_pair("c", "bla", "black")
        assert not removed_from_pair("c", "bla", "blak")
    
    def test_probability_blank(self):
        result = abbreviation_probability("", "captain")
        assert result == 0.0
    
    def test_probability(self):
        result = abbreviation_probability("cptn", "captain")
        assert round(result, 2) == 1.0
        result = abbreviation_probability("captn", "captain")
        assert round(result, 2) == 0.44
        result = abbreviation_probability("dmrl", "admiral")
        assert round(result, 2) == 0.5
        result = abbreviation_probability("admrl", "admiral")
        assert round(result, 2) == 1.0
        result = abbreviation_probability("admirl", "admiral")
        assert round(result, 2) == 0.25
        result = abbreviation_probability("admr", "admiral")
        assert round(result, 2) == 0.11
        result = abbreviation_probability("admrial", "admiral")
        assert round(result, 2) == 0.0
        result = abbreviation_probability("admairal", "admiral")
        assert round(result, 2) == 0.0
        result = abbreviation_probability("blc", "black")
        assert round(result, 2) == 1.0


class TestTypo():        
    def test_probability_blank(self):
        result = typo_probability("", "captain")
        assert result == 0.0
    
    def test_probability(self):
        result = typo_probability("testing", "testing")
        assert round(result, 2) == 0.0
        result = typo_probability("testinge", "testing")
        assert round(result, 2) == 0.5
        result = typo_probability("testin", "testing")
        assert round(result, 2) == 0.5
        result = typo_probability("tseting", "testing")
        assert round(result, 2) == 1.0
        result = typo_probability("seetting", "setting")
        assert round(result, 2) == 1.0
        result = typo_probability("seting", "setting")
        assert round(result, 2) == 1.0
        result = typo_probability("setinge", "setting")
        assert round(result, 2) == 0.5
        result = typo_probability("setinged", "setting")
        assert round(result, 2) == 0.21
        
        result = typo_probability("testingZ", "testing", tn_edits=edits("testingZ", "testing", ignore_case=False))
        assert round(result, 2) < 0.5


class TestBrand():        
    def test_probability_blank(self):
        result = brand_probability("", "Microsoft")
        assert result == 0.0
        
    def test_probability_no_duplications(self):
        result = brand_probability("Lazzer", "Lazer", edits("Lazzer", "Lazer"))
        assert round(result, 2) == 0.75
        result = brand_probability("Haxxor", "Haxor", edits("Lazzer", "Lazer"))
        assert round(result, 2) == 0.75
    
    def test_probability(self):
        result = brand_probability("Lazzer", "Lazer")
        assert round(result, 2) == 0.75
        result = brand_probability("SpaceX", "Space")
        assert round(result, 2) == 0.75
        result = brand_probability("Protox", "Proto")
        assert round(result, 2) == 0.75
        result = brand_probability("Braidz", "Braids")
        assert round(result, 2) == 1.0
        result = brand_probability("Braidz", "Braid")
        assert round(result, 2) == 0.75
        result = brand_probability("T-Mobile", "Mobile")
        assert round(result, 2) == 1.0
        result = brand_probability("TMobile", "Mobile")
        assert round(result, 2) == 1.0
        result = brand_probability("Playmobil", "Playmobile")
        assert round(result, 2) == 0.5
        result = brand_probability("Pak", "Pack")
        assert round(result, 2) == 0.75
        result = brand_probability("Pac", "Pack")
        assert round(result, 2) == 0.75
        result = brand_probability("Tek", "Tech")
        assert round(result, 2) == 0.75
        result = brand_probability("ProMariner", "Mariner")
        assert round(result, 2) == 1.0
        result = brand_probability("PaperPro", "Paper")
        assert round(result, 2) == 1.0
        result = brand_probability("Telefoto", "Telephoto")
        assert round(result, 2) == 0.75
        result = brand_probability("Savvy", "Savy")
        assert round(result, 2) == 0.75
        result = brand_probability("Exxcel", "Excel")
        assert round(result, 2) == 0.75
        result = brand_probability("Binkie", "Binky")
        assert round(result, 2) == 0.75


class TestClassification():
    def test_unknown(self):
        assert Changes_Classification('Vneck', 'VANEYCK') == ['Unknown', 'Unknown']
        assert Changes_Classification('Summner', 'SUMMER') == ['Unknown']
        assert Changes_Classification('Howllow', 'Hollow') == ['Unknown']
        assert Changes_Classification('Gastby', 'Gatsby') == ['Typo']
        assert Changes_Classification('Classicial', 'Classical') == ['Unknown']
        assert Changes_Classification('VOGTAGE', 'VOLTAGE') == ['Unknown']
        assert Changes_Classification('Ajduster', 'Adjuster') == ['Typo']
        assert Changes_Classification('Altantic', 'Atlantic') == ['Typo']
        assert Changes_Classification('Aqualon', 'Aqualyn') == ['Unknown']
        assert Changes_Classification('Beckect', 'Becket') == ['Unknown']
        assert Changes_Classification('Bouyancy', 'Buoyancy') == ['Typo']
        assert Changes_Classification('Braket', 'Brake') == ['Unknown']
        assert Changes_Classification('Crugear', 'Cruger') == ['Unknown']
        assert Changes_Classification('Cupler', 'Cuyler') == ['Unknown']
        assert Changes_Classification('Dufflel', 'Duffel') == ['Unknown']
        assert Changes_Classification('Dufflel', 'Duffle') == ['Unknown']
        assert Changes_Classification('Elastromer', 'Elastomer') == ['Unknown']
        assert Changes_Classification('Firfely', 'Firefly') == ['Typo']
        assert Changes_Classification('Flairlead', 'Fairlead') == ['Unknown']
        assert Changes_Classification('Furlex', 'Furler') == ['Unknown']
        assert Changes_Classification('Gaurds', 'Guards') == ['Typo']
        assert Changes_Classification('Houseing', 'Housing') == ['Unknown']
        assert Changes_Classification('Kanvas', 'Kansas') == ['Unknown']
        assert Changes_Classification('LaserA', 'Laser') == ['Unknown']
        assert Changes_Classification('Lloyed', 'Lloyd') == ['Unknown']
        assert Changes_Classification('Melges', 'meles') == ['Unknown']
        assert Changes_Classification('Mohogany', 'Mahogany') == ['Unknown']
        assert Changes_Classification('Nylone', 'Nylon') == ['Tense', 'Unknown']
        assert Changes_Classification('Pinstop', 'pitstop') == ['Unknown']
        assert Changes_Classification('Pivoiting', 'Pivoting') == ['Unknown']
        assert Changes_Classification('Sahckle', 'Shackle') == ['Typo']
        assert Changes_Classification('Serier', 'Series') == ['Unknown']
        assert Changes_Classification('Serires', 'Series') == ['Unknown']
        assert Changes_Classification('Sharki', 'shaki') == ['Unknown']
        assert Changes_Classification('Sharki', 'Shari') == ['Unknown']
        assert Changes_Classification('Sharki', 'Shark') == ['Unknown']
        assert Changes_Classification('Sharktooth', 'Sharptooth') == ['Unknown']
        assert Changes_Classification('Shorti', 'Short') == ['Unknown']
        assert Changes_Classification('Shorti', 'Shorts') == ['Unknown']
        assert Changes_Classification('Sprial', 'Spiral') == ['Typo']
        assert Changes_Classification('Stainloess', 'Stainless') == ['Unknown']
        assert Changes_Classification('Stanless', 'Stanleys') == ['Unknown']
        assert Changes_Classification('SunfishA', 'Sunfish') == ['Unknown']
        assert Changes_Classification('Tenion', 'Tenino') == ['Typo']
        assert Changes_Classification('Tenion', 'Tenon') == ['Unknown']
        assert Changes_Classification('Thmible', 'Thimble') == ['Typo']
        assert Changes_Classification('Torlon', 'Toulon') == ['Unknown']
        assert Changes_Classification('Trackw', 'Track') == ['Unknown']
        assert Changes_Classification('Trailex', 'Trailer') == ['Unknown']
        assert Changes_Classification('Univeresal', 'Universal') == ['Unknown']
        assert Changes_Classification('Dble', 'DYABLE') == ['Unknown', 'Unknown']
        assert Changes_Classification('Dble', 'DYEABLE') == ['Unknown', 'Unknown', 'Unknown']
        assert Changes_Classification('Upstd', 'UPSTAYED') == ['Unknown', 'Unknown', 'Unknown']
        assert Changes_Classification('Blc', 'Black') == ['Unknown', 'Unknown']

    def test_abbr(self):
        assert Changes_Classification('Summner', 'SUMMONER') == ['Abbreviation']
        assert Changes_Classification('Slvless', 'Sleeveless') == ['Abbreviation']
        assert Changes_Classification('Anod', 'ANODE') == ['Tense']
        assert Changes_Classification('Anodz', 'ANODIZE') == ['Abbreviation']
        assert Changes_Classification('Assy', 'ASSAY') == ['Abbreviation']
        assert Changes_Classification('Assy', 'assaye') == ['Abbreviation']
        assert Changes_Classification('Beckt', 'Becket') == ['Abbreviation']
        assert Changes_Classification('Brait', 'baraita') == ['Abbreviation']
        assert Changes_Classification('Brckt', 'Bracket') == ['Abbreviation']
        assert Changes_Classification('Brckt', 'BROCKET') == ['Abbreviation']
        assert Changes_Classification('Brckt', 'BROCKIT') == ['Abbreviation']
        assert Changes_Classification('Brkt', 'BARAKAT') == ['Abbreviation']
        assert Changes_Classification('Brkt', 'barkat') == ['Abbreviation']
        assert Changes_Classification('Brkt', 'BARKET') == ['Abbreviation']
        assert Changes_Classification('Brkt', 'Burket') == ['Abbreviation']
        assert Changes_Classification('Brkt', 'BURKITE') == ['Abbreviation']
        assert Changes_Classification('Cluch', 'CELUCH') == ['Abbreviation']
        assert Changes_Classification('Cluch', 'CLEUCH') == ['Abbreviation']
        assert Changes_Classification('Cntr', 'CANTAR') == ['Abbreviation']
        assert Changes_Classification('Cntr', 'CANTARA') == ['Abbreviation']
        assert Changes_Classification('Cntr', 'CANTER') == ['Abbreviation']
        assert Changes_Classification('Cntr', 'CANTERO') == ['Abbreviation']
        assert Changes_Classification('Cntr', 'CANTIER') == ['Abbreviation']
        assert Changes_Classification('Cntr', 'CANTOR') == ['Abbreviation']
        assert Changes_Classification('Cntr', 'CANTORE') == ['Abbreviation']
        assert Changes_Classification('Cntr', 'CANTRE') == ['Abbreviation']
        assert Changes_Classification('Cntr', 'CANTRIO') == ['Abbreviation']
        assert Changes_Classification('Cntr', 'CENTARE') == ['Abbreviation']
        assert Changes_Classification('Cntr', 'CENTAUR') == ['Abbreviation']
        assert Changes_Classification('Cntr', 'Center') == ['Abbreviation']
        assert Changes_Classification('Cntr', 'CENTORE') == ['Abbreviation']
        assert Changes_Classification('Cntr', 'CENTRA') == ['Abbreviation']
        assert Changes_Classification('Cntr', 'CENTRE') == ['Abbreviation']
        assert Changes_Classification('Cntr', 'CENTRY') == ['Abbreviation']
        assert Changes_Classification('Cntr', 'Centura') == ['Abbreviation']
        assert Changes_Classification('Cntr', 'CENTURY') == ['Abbreviation']
        assert Changes_Classification('Cntr', 'CINTORA') == ['Abbreviation']
        assert Changes_Classification('Cntr', 'CINTRA') == ['Abbreviation']
        assert Changes_Classification('Cntr', 'COINTER') == ['Abbreviation']
        assert Changes_Classification('Cntr', 'CONTOUR') == ['Abbreviation']
        assert Changes_Classification('Cntr', 'contr') == ['Abbreviation']
        assert Changes_Classification('Cntr', 'CONTRA') == ['Abbreviation']
        assert Changes_Classification('Cntr', 'contre') == ['Abbreviation']
        assert Changes_Classification('Cntr', 'contro') == ['Abbreviation']
        assert Changes_Classification('Cntr', 'COUNTER') == ['Abbreviation']
        assert Changes_Classification('Cntr', 'COUNTRY') == ['Abbreviation']
        assert Changes_Classification('Cntrl', 'CANTORAL') == ['Abbreviation']
        assert Changes_Classification('Cntrl', 'CANTREL') == ['Abbreviation']
        assert Changes_Classification('Cntrl', 'Cantril') == ['Abbreviation']
        assert Changes_Classification('Cntrl', 'CENTRAL') == ['Abbreviation']
        assert Changes_Classification('Cntrl', 'centrale') == ['Abbreviation']
        assert Changes_Classification('Cntrl', 'CONTRAIL') == ['Abbreviation']
        assert Changes_Classification('Cntrl', 'Control') == ['Abbreviation']
        assert Changes_Classification('Cntrl', 'CONTROLE') == ['Abbreviation']
        assert Changes_Classification('Cntrl', 'CONTROUL') == ['Abbreviation']
        assert Changes_Classification('Cntrl', 'COUNTROL') == ['Abbreviation']
        assert Changes_Classification('Composit', 'Composite') == ['Tense']
        assert Changes_Classification('Continous', 'Continuous') == ['Abbreviation']
        assert Changes_Classification('Cotz', 'coetzee') == ['Abbreviation']
        assert Changes_Classification('Cupler', 'Coupler') == ['Abbreviation']
        assert Changes_Classification('Cupler', 'CUPELER') == ['Abbreviation']
        assert Changes_Classification('Dble', 'DEBILE') == ['Abbreviation']
        assert Changes_Classification('Dble', 'DEIBLE') == ['Abbreviation']
        assert Changes_Classification('Dble', 'DIABLE') == ['Abbreviation']
        assert Changes_Classification('Dble', 'DIBLE') == ['Abbreviation']
        assert Changes_Classification('Dble', 'DIBLEY') == ['Unknown', 'Unknown']
        assert Changes_Classification('Dble', 'DOABLE') == ['Abbreviation']
        assert Changes_Classification('Dble', 'DOBLE') == ['Abbreviation']
        assert Changes_Classification('Dble', 'Double') == ['Abbreviation']
        assert Changes_Classification('Dble', 'DUBLE') == ['Abbreviation']
        assert Changes_Classification('Deadend', 'DEADENED') == ['Abbreviation']
        assert Changes_Classification('Delux', 'Deluxe') == ['Tense']
        assert Changes_Classification('Fastners', 'Fasteners') == ['Abbreviation']
        assert Changes_Classification('Fluro', 'fluoro') == ['Abbreviation']
        assert Changes_Classification('Frac', 'FARACE') == ['Abbreviation']
        assert Changes_Classification('Frac', 'FARACI') == ['Abbreviation']
        assert Changes_Classification('Frac', 'FARACO') == ['Abbreviation']
        assert Changes_Classification('Frac', 'FERACO') == ['Abbreviation']
        assert Changes_Classification('Fsteners', 'Fasteners') == ['Abbreviation']
        assert Changes_Classification('Goosneck', 'Gooseneck') == ['Abbreviation']
        assert Changes_Classification('Goseneck', 'Gooseneck') == ['Abbreviation']
        assert Changes_Classification('Gudg', 'GAUDGIE') == ['Abbreviation']
        assert Changes_Classification('Gudg', 'GOUDGE') == ['Abbreviation']
        assert Changes_Classification('Gudg', 'GUIDAGE') == ['Abbreviation']
        assert Changes_Classification('Hatchs', 'Hatches') == ['Abbreviation']
        assert Changes_Classification('Jackline', 'JACKELINE') == ['Abbreviation']
        assert Changes_Classification('lgth', 'LEGATH') == ['Abbreviation']
        assert Changes_Classification('Lngth', 'Length') == ['Abbreviation']
        assert Changes_Classification('Lngth', 'LENGTHY') == ['Abbreviation']
        assert Changes_Classification('Lume', 'LAUMEA') == ['Abbreviation']
        assert Changes_Classification('Matic', 'MAIEUTIC') == ['Abbreviation']
        assert Changes_Classification('Matic', 'matica') == ['Abbreviation']
        assert Changes_Classification('Matic', 'MATICE') == ['Tense']
        assert Changes_Classification('Matic', 'MATICO') == ['Abbreviation']
        assert Changes_Classification('pigt', 'piaget') == ['Abbreviation']
        assert Changes_Classification('pigt', 'PIGAT') == ['Abbreviation']
        assert Changes_Classification('pigt', 'pigot') == ['Abbreviation']
        assert Changes_Classification('pigt', 'PIGOUT') == ['Abbreviation']
        assert Changes_Classification('pigt', 'PIGUET') == ['Abbreviation']
        assert Changes_Classification('Pinst', 'PIANIST') == ['Abbreviation', 'Typo']
        assert Changes_Classification('Pinst', 'PIANISTE') == ['Abbreviation', 'Typo']
        assert Changes_Classification('Pinst', 'PINIEST') == ['Abbreviation']
        assert Changes_Classification('Plgr', 'peligro') == ['Abbreviation']
        assert Changes_Classification('Plgr', 'Pilger') == ['Abbreviation']
        assert Changes_Classification('Plgr', 'PLAGUER') == ['Abbreviation']
        assert Changes_Classification('Plgr', 'PLOEGER') == ['Abbreviation']
        assert Changes_Classification('Plgr', 'PLUEGER') == ['Abbreviation']
        assert Changes_Classification('Plgr', 'polgar') == ['Abbreviation']
        assert Changes_Classification('Polypropylen', 'Polypropylene') == ['Tense']
        assert Changes_Classification('Remov', 'REMOVE') == ['Tense']
        assert Changes_Classification('Replac', 'Replace') == ['Tense']
        assert Changes_Classification('Replacment', 'Replacement') == ['Abbreviation']
        assert Changes_Classification('Replc', 'Replace') == ['Abbreviation']
        assert Changes_Classification('Replc', 'REPLICA') == ['Abbreviation']
        assert Changes_Classification('Rodk', 'RODAK') == ['Abbreviation']
        assert Changes_Classification('Rodk', 'RODKEY') == ['Truncation']
        assert Changes_Classification('Salti', 'SALTIE') == ['Tense']
        assert Changes_Classification('Senstive', 'Sensitive') == ['Abbreviation']
        assert Changes_Classification('Sgls', 'SEGOLS') == ['Abbreviation']
        assert Changes_Classification('Sgls', 'SIGILS') == ['Abbreviation']
        assert Changes_Classification('Sgls', 'SIGLAS') == ['Abbreviation']
        assert Changes_Classification('Sgls', 'SIGLOS') == ['Abbreviation']
        assert Changes_Classification('Sheav', 'Sheave') == ['Tense']
        assert Changes_Classification('sheve', 'Sheave') == ['Abbreviation']
        assert Changes_Classification('sheve', 'SHEEVE') == ['Abbreviation']
        assert Changes_Classification('sheve', 'SHEIVE') == ['Abbreviation']
        assert Changes_Classification('Shorti', 'SHORTIA') == ['Abbreviation']
        assert Changes_Classification('Shorti', 'Shortie') == ['Tense']
        assert Changes_Classification('Shve', 'SHAVE') == ['Abbreviation']
        assert Changes_Classification('Shve', 'SHAVIE') == ['Abbreviation']
        assert Changes_Classification('Shve', 'Sheave') == ['Abbreviation']
        assert Changes_Classification('Shve', 'SHEEVE') == ['Abbreviation']
        assert Changes_Classification('Shve', 'SHEIVE') == ['Abbreviation']
        assert Changes_Classification('Shve', 'SHIVE') == ['Abbreviation']
        assert Changes_Classification('Shve', 'SHOVE') == ['Abbreviation']
        assert Changes_Classification('Shve', 'SHVEY') == ['Unknown']
        assert Changes_Classification('Sngl', 'sangala') == ['Abbreviation']
        assert Changes_Classification('Sngl', 'sangley') == ['Abbreviation']
        assert Changes_Classification('Sngl', 'sangli') == ['Abbreviation']
        assert Changes_Classification('Sngl', 'Senegal') == ['Abbreviation']
        assert Changes_Classification('Sngl', 'SINEGAL') == ['Abbreviation']
        assert Changes_Classification('Sngl', 'Singl') == ['Abbreviation']
        assert Changes_Classification('Sngl', 'Single') == ['Abbreviation']
        assert Changes_Classification('Sngl', 'SINGLEY') == ['Abbreviation']
        assert Changes_Classification('Sngl', 'SINGLY') == ['Abbreviation']
        assert Changes_Classification('Sngl', 'SNUGLY') == ['Abbreviation']
        assert Changes_Classification('Stanless', 'Stainless') == ['Abbreviation']
        assert Changes_Classification('Strght', 'Straight') == ['Abbreviation']
        assert Changes_Classification('Strght', 'STRAUGHT') == ['Abbreviation']
        assert Changes_Classification('Strght', 'STREIGHT') == ['Abbreviation']
        assert Changes_Classification('Strght', 'STRIGHT') == ['Abbreviation']
        assert Changes_Classification('Swvl', 'SWAVELY') == ['Abbreviation']
        assert Changes_Classification('Swvl', 'Swivel') == ['Abbreviation']
        assert Changes_Classification('Telo', 'TELOI') == ['Abbreviation']
        assert Changes_Classification('Towabl', 'Towable') == ['Tense']
        assert Changes_Classification('Trav', 'TRAVE') == ['Abbreviation']
        assert Changes_Classification('Trilite', 'TROILITE') == ['Abbreviation']
        assert Changes_Classification('Twing', 'TAWING') == ['Abbreviation']
        assert Changes_Classification('Twing', 'TEWING') == ['Abbreviation']
        assert Changes_Classification('Twing', 'Towing') == ['Abbreviation']
        assert Changes_Classification('Twing', 'TWINGE') == ['Tense']
        assert Changes_Classification('Univers', 'UNIVERSE') == ['Abbreviation']
        assert Changes_Classification('Univers', 'universo') == ['Abbreviation']
        assert Changes_Classification('Upstd', 'UPSTOOD') == ['Abbreviation']
        assert Changes_Classification('Upstnd', 'Upstand') == ['Abbreviation']

    def test_typo(self):
        assert Changes_Classification('Daallas', 'Dallas') == ['Typo']
        assert Changes_Classification('Dallas', 'Dalas') == ['Typo']
        assert Changes_Classification('Mist', 'Nist') == ['Typo']
        assert Changes_Classification('Shenendoah', 'Shenandoah') == ['Typo']
        assert Changes_Classification('Shenendoa', 'Shenandoah') == ['Typo', 'Unknown']
        assert Changes_Classification('Shenendoah', 'Shenantoah') == ['Typo', 'Typo']
        assert Changes_Classification('Mellon', 'Melo') == ['Typo', 'Unknown']
        assert Changes_Classification('cat', 'vat') == ['Typo']
        assert Changes_Classification('Acetel', 'Acetal') == ['Typo']
        assert Changes_Classification('Furlex', 'Furled') == ['Typo']
        assert Changes_Classification('LaserA', 'Lasers') == ['Typo']
        assert Changes_Classification('Protex', 'Propex') == ['Typo']
        assert Changes_Classification('Protex', 'Protem') == ['Typo']
        assert Changes_Classification('Sparcraft', 'starcraft') == ['Typo']
        assert Changes_Classification('Sumbrella', 'Sunbrella') == ['Typo']
        assert Changes_Classification('Threead', 'Thread') == ['Typo']
        assert Changes_Classification('Tutnbuckle', 'Turnbuckle') == ['Typo']
        assert Changes_Classification('Windshift', 'Windshirt') == ['Typo']

    def test_truncation(self):
        assert Changes_Classification('Med', 'Medium') == ['Truncation']
        assert Changes_Classification('Trackw', 'TRACKWAY') == ['Truncation']

    def test_singular(self):
        assert Changes_Classification('cat', 'cats') == ['Singular']

    def test_plural(self):
        assert Changes_Classification('books', 'book') == ['Plural']
        assert Changes_Classification('Hatchs', 'Hatch') == ['Plural']
        assert Changes_Classification('Helmsmans', 'Helmsman') == ['Plural']
        assert Changes_Classification('Kanvas', 'kanva') == ['Plural']


class TestProbabilities():
    def test_different(self):
        x = dict(Probabilities('Vneck', 'VANEYCK', i_round=2)._asdict())
        assert x == {'singular': 0.0, 'plural': 0.0, 'tense': 0.0, 'typo': 0.21, 'abbreviation': 0.04, 'truncation': 0.0, 'brand': 0.0}
        x = dict(Probabilities('Summner', 'SUMMONER', i_round=2)._asdict())
        assert x == {'singular': 0.0, 'plural': 0.0, 'tense': 0.0, 'typo': 0.5, 'abbreviation': 0.11, 'truncation': 0.0, 'brand': 0.0}
        x = dict(Probabilities('Summner', 'SUMMER', i_round=2)._asdict())
        assert x == {'singular': 0.0, 'plural': 0.0, 'tense': 0.0, 'typo': 0.5, 'abbreviation': 0.0, 'truncation': 0.0, 'brand': 0.0}
        x = dict(Probabilities('Slvless', 'Sleeveless', i_round=2)._asdict())
        assert x == {'singular': 0.0, 'plural': 0.0, 'tense': 0.0, 'typo': 0.21, 'abbreviation': 0.56, 'truncation': 0.0, 'brand': 0.0}
        x = dict(Probabilities('Howllow', 'Hollow', i_round=2)._asdict())
        assert x == {'singular': 0.0, 'plural': 0.0, 'tense': 0.0, 'typo': 0.5, 'abbreviation': 0.0, 'truncation': 0.0, 'brand': 0.0}
        x = dict(Probabilities('Classicial', 'Classical', i_round=2)._asdict())
        assert x == {'singular': 0.0, 'plural': 0.0, 'tense': 0.0, 'typo': 0.5, 'abbreviation': 0.0, 'truncation': 0.0, 'brand': 0.0}
        x = dict(Probabilities('VOGTAGE', 'VOLTAGE', i_round=2)._asdict())
        assert x == {'singular': 0.0, 'plural': 0.0, 'tense': 0.0, 'typo': 0.5, 'abbreviation': 0.0, 'truncation': 0.0, 'brand': 0.0}
        
        
    def test_typo(self):
        x = dict(Probabilities('Gastby', 'Gatsby', i_round=2)._asdict())
        assert x == {'singular': 0.0, 'plural': 0.0, 'tense': 0.0, 'typo': 1.0, 'abbreviation': 0.0, 'truncation': 0.0, 'brand': 0.0}
        x = dict(Probabilities('Daallas', 'Dallas', i_round=2)._asdict())
        assert x == {'singular': 0.0, 'plural': 0.0, 'tense': 0.0, 'typo': 1.0, 'abbreviation': 0.0, 'truncation': 0.0, 'brand': 0.0}
        x = dict(Probabilities('Dallas', 'Dalas', i_round=2)._asdict())
        assert x == {'singular': 0.0, 'plural': 0.0, 'tense': 0.0, 'typo': 1.0, 'abbreviation': 0.0, 'truncation': 0.0, 'brand': 0.0}
        x = dict(Probabilities('Mist', 'Nist', i_round=2)._asdict())
        assert x == {'singular': 0.0, 'plural': 0.0, 'tense': 0.0, 'typo': 0.5, 'abbreviation': 0.0, 'truncation': 0.0, 'brand': 0.0}
        x = dict(Probabilities('Shenendoah', 'Shenandoah', i_round=2)._asdict())
        assert x == {'singular': 0.0, 'plural': 0.0, 'tense': 0.0, 'typo': 1.0, 'abbreviation': 0.0, 'truncation': 0.0, 'brand': 0.0}
        x = dict(Probabilities('Shenendoah', 'Shenantoah', i_round=2)._asdict())
        assert x == {'singular': 0.0, 'plural': 0.0, 'tense': 0.0, 'typo': 0.37, 'abbreviation': 0.0, 'truncation': 0.0, 'brand': 0.0}
        x = dict(Probabilities('cat', 'vat', i_round=2)._asdict())
        assert x == {'singular': 0.0, 'plural': 0.0, 'tense': 0.0, 'typo': 0.5, 'abbreviation': 0.0, 'truncation': 0.0, 'brand': 0.0}
        x = dict(Probabilities("startin'", 'starting', i_round=2)._asdict())
        assert x == {'singular': 0.0, 'plural': 0.0, 'tense': 0.0, 'typo': 0.5, 'abbreviation': 0.0, 'truncation': 0.0, 'brand': 0.0}
        
    def test_truncation(self):
        x = dict(Probabilities('Med', 'Medium', i_round=2)._asdict())
        assert x == {'singular': 0.0, 'plural': 0.0, 'tense': 0.0, 'typo': 0.0, 'abbreviation': 0.06, 'truncation': 1.0, 'brand': 0.0}
        x = dict(Probabilities('Capt', 'Captain', i_round=2)._asdict())
        assert x == {'singular': 0.0, 'plural': 0.0, 'tense': 0.0, 'typo': 0.0, 'abbreviation': 0.06, 'truncation': 1.0, 'brand': 0.0}
        x = dict(Probabilities('Cpl', 'Corporal', i_round=2)._asdict())
        assert x == {'singular': 0.0, 'plural': 0.0, 'tense': 0.0, 'typo': 0.0, 'abbreviation': 0.04, 'truncation': 0.0, 'brand': 0.0}
        x = dict(Probabilities('Trackw', 'TRACKWAY', i_round=2)._asdict())
        assert x == {'singular': 0.0, 'plural': 0.0, 'tense': 0.0, 'typo': 0.21, 'abbreviation': 0.04, 'truncation': 0.0, 'brand': 0.0}
    
    def test_abbreviation(self):
        x = dict(Probabilities('Blc', 'Black', i_round=2)._asdict())
        assert x == {'singular': 0.0, 'plural': 0.0, 'tense': 0.0, 'typo': 0.21, 'abbreviation': 1.0, 'truncation': 0.0, 'brand': 0.0}
        x = dict(Probabilities('Blk', 'Black', i_round=2)._asdict())
        assert x == {'singular': 0.0, 'plural': 0.0, 'tense': 0.0, 'typo': 0.21, 'abbreviation': 1.0, 'truncation': 0.0, 'brand': 0.0}
        x = dict(Probabilities('Anod', 'ANODE', i_round=2)._asdict())
        assert x == {'singular': 0.0, 'plural': 0.0, 'tense': 1.0, 'typo': 0.0, 'abbreviation': 0.0, 'truncation': 0.0, 'brand': 0.0}
        x = dict(Probabilities('Anodz', 'ANODIZE', i_round=2)._asdict())
        assert x == {'singular': 0.0, 'plural': 0.0, 'tense': 0.0, 'typo': 0.21, 'abbreviation': 0.44, 'truncation': 0.0, 'brand': 0.0}
        x = dict(Probabilities('Assy', 'ASSAY', i_round=2)._asdict())
        assert x == {'singular': 0.0, 'plural': 0.0, 'tense': 0.0, 'typo': 0.5, 'abbreviation': 0.25, 'truncation': 0.0, 'brand': 0.0}
        
    def test_singular(self):
        x = dict(Probabilities('cat', 'cats', i_round=2)._asdict())
        assert x == {'singular': 1.0, 'plural': 0.0, 'tense': 0.0, 'typo': 0.0, 'abbreviation': 0.0, 'truncation': 0.0, 'brand': 0.0}
        x = dict(Probabilities('catch', 'catches', i_round=2)._asdict())
        assert x == {'singular': 1.0, 'plural': 0.0, 'tense': 0.0, 'typo': 0.0, 'abbreviation': 0.0, 'truncation': 0.0, 'brand': 0.0}
        x = dict(Probabilities('fish', 'fish', i_round=2)._asdict())
        assert x == {'singular': 0.0, 'plural': 0.0, 'tense': 0.0, 'typo': 0.0, 'abbreviation': 0.0, 'truncation': 0.0, 'brand': 0.0}
        
    def test_plural(self):
        x = dict(Probabilities('books', 'book', i_round=2)._asdict())
        assert x == {'singular': 0.0, 'plural': 1.0, 'tense': 0.0, 'typo': 0.0, 'abbreviation': 0.0, 'truncation': 0.0, 'brand': 0.0}
        x = dict(Probabilities('deer', 'deer', i_round=2)._asdict())
        assert x == {'singular': 0.0, 'plural': 0.0, 'tense': 0.0, 'typo': 0.0, 'abbreviation': 0.0, 'truncation': 0.0, 'brand': 0.0}

'''
        x = dict(Probabilities('Acetel', 'Acetal', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 1.0}
        x = dict(Probabilities('Ajduster', 'Adjuster', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 1.0}
        x = dict(Probabilities('Altantic', 'Atlantic', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 1.0}
        x = dict(Probabilities('Aqualon', 'Aqualyn', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Beckect', 'Becket', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Bouyancy', 'Buoyancy', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 1.0}
        x = dict(Probabilities('Braket', 'Brake', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Crugear', 'Cruger', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Cupler', 'Cuyler', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Dufflel', 'Duffel', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Dufflel', 'Duffle', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Elastromer', 'Elastomer', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Firfely', 'Firefly', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 1.0}
        x = dict(Probabilities('Flairlead', 'Fairlead', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Furlex', 'Furled', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 1.0}
        x = dict(Probabilities('Furlex', 'Furler', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Gaurds', 'Guards', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 1.0}
        x = dict(Probabilities('Hatchs', 'Hatch', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 1.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Helmsmans', 'Helmsman', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 1.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Houseing', 'Housing', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Kanvas', 'Kansas', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Kanvas', 'kanva', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 1.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('LaserA', 'Laser', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('LaserA', 'Lasers', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 1.0}
        x = dict(Probabilities('Lloyed', 'Lloyd', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Melges', 'meles', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Mohogany', 'Mahogany', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Nylone', 'Nylon', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Pinstop', 'pitstop', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Pivoiting', 'Pivoting', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Protex', 'Propex', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 1.0}
        x = dict(Probabilities('Protex', 'Protem', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 1.0}
        x = dict(Probabilities('Sahckle', 'Shackle', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 1.0}
        x = dict(Probabilities('Serier', 'Series', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Serires', 'Series', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Sharki', 'shaki', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Sharki', 'Shari', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Sharki', 'Shark', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Sharktooth', 'Sharptooth', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Shorti', 'Short', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Shorti', 'Shorts', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Sparcraft', 'starcraft', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 1.0}
        x = dict(Probabilities('Sprial', 'Spiral', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 1.0}
        x = dict(Probabilities('Stainloess', 'Stainless', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Stanless', 'Stanleys', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Sumbrella', 'Sunbrella', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 1.0}
        x = dict(Probabilities('SunfishA', 'Sunfish', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Tenion', 'Tenino', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 1.0}
        x = dict(Probabilities('Tenion', 'Tenon', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Thmible', 'Thimble', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 1.0}
        x = dict(Probabilities('Threead', 'Thread', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 1.0}
        x = dict(Probabilities('Torlon', 'Toulon', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Trackw', 'Track', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Trailex', 'Trailer', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Tutnbuckle', 'Turnbuckle', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 1.0}
        x = dict(Probabilities('Univeresal', 'Universal', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Windshift', 'Windshirt', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 1.0}
        x = dict(Probabilities('Assy', 'assaye', i_round=2)._asdict())
        assert x == {'abbreviation': 0.12, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Beckt', 'Becket', i_round=2)._asdict())
        assert x == {'abbreviation': 0.11, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Brait', 'baraita', i_round=2)._asdict())
        assert x == {'abbreviation': 0.12, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Brckt', 'Bracket', i_round=2)._asdict())
        assert x == {'abbreviation': 0.22, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Brckt', 'BROCKET', i_round=2)._asdict())
        assert x == {'abbreviation': 0.22, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Brckt', 'BROCKIT', i_round=2)._asdict())
        assert x == {'abbreviation': 0.22, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Brkt', 'BARAKAT', i_round=2)._asdict())
        assert x == {'abbreviation': 0.33, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Brkt', 'barkat', i_round=2)._asdict())
        assert x == {'abbreviation': 0.50, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Brkt', 'BARKET', i_round=2)._asdict())
        assert x == {'abbreviation': 0.50, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Brkt', 'Burket', i_round=2)._asdict())
        assert x == {'abbreviation': 0.50, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Brkt', 'BURKITE', i_round=2)._asdict())
        assert x == {'abbreviation': 0.33, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Cluch', 'CELUCH', i_round=2)._asdict())
        assert x == {'abbreviation': 0.11, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Cluch', 'CLEUCH', i_round=2)._asdict())
        assert x == {'abbreviation': 0.11, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Cntr', 'CANTAR', i_round=2)._asdict())
        assert x == {'abbreviation': 0.50, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Cntr', 'CANTARA', i_round=2)._asdict())
        assert x == {'abbreviation': 0.33, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Cntr', 'CANTER', i_round=2)._asdict())
        assert x == {'abbreviation': 0.50, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Cntr', 'CANTERO', i_round=2)._asdict())
        assert x == {'abbreviation': 0.33, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Cntr', 'CANTIER', i_round=2)._asdict())
        assert x == {'abbreviation': 0.33, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Cntr', 'CANTOR', i_round=2)._asdict())
        assert x == {'abbreviation': 0.50, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Cntr', 'CANTORE', i_round=2)._asdict())
        assert x == {'abbreviation': 0.33, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Cntr', 'CANTRE', i_round=2)._asdict())
        assert x == {'abbreviation': 0.50, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Cntr', 'CANTRIO', i_round=2)._asdict())
        assert x == {'abbreviation': 0.33, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Cntr', 'CENTARE', i_round=2)._asdict())
        assert x == {'abbreviation': 0.33, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Cntr', 'CENTAUR', i_round=2)._asdict())
        assert x == {'abbreviation': 0.33, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Cntr', 'Center', i_round=2)._asdict())
        assert x == {'abbreviation': 0.50, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Cntr', 'CENTORE', i_round=2)._asdict())
        assert x == {'abbreviation': 0.33, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Cntr', 'CENTRA', i_round=2)._asdict())
        assert x == {'abbreviation': 0.50, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Cntr', 'CENTRE', i_round=2)._asdict())
        assert x == {'abbreviation': 0.50, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Cntr', 'CENTRY', i_round=2)._asdict())
        assert x == {'abbreviation': 0.11, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Cntr', 'Centura', i_round=2)._asdict())
        assert x == {'abbreviation': 0.33, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Cntr', 'CENTURY', i_round=2)._asdict())
        assert x == {'abbreviation': 0.12, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Cntr', 'CINTORA', i_round=2)._asdict())
        assert x == {'abbreviation': 0.33, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Cntr', 'CINTRA', i_round=2)._asdict())
        assert x == {'abbreviation': 0.50, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Cntr', 'COINTER', i_round=2)._asdict())
        assert x == {'abbreviation': 0.33, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Cntr', 'CONTOUR', i_round=2)._asdict())
        assert x == {'abbreviation': 0.33, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Cntr', 'contr', i_round=2)._asdict())
        assert x == {'abbreviation': 1.00, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Cntr', 'CONTRA', i_round=2)._asdict())
        assert x == {'abbreviation': 0.50, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Cntr', 'contre', i_round=2)._asdict())
        assert x == {'abbreviation': 0.50, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Cntr', 'contro', i_round=2)._asdict())
        assert x == {'abbreviation': 0.50, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Cntr', 'COUNTER', i_round=2)._asdict())
        assert x == {'abbreviation': 0.33, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Cntr', 'COUNTRY', i_round=2)._asdict())
        assert x == {'abbreviation': 0.12, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Cntrl', 'CANTORAL', i_round=2)._asdict())
        assert x == {'abbreviation': 0.33, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Cntrl', 'CANTREL', i_round=2)._asdict())
        assert x == {'abbreviation': 0.50, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Cntrl', 'Cantril', i_round=2)._asdict())
        assert x == {'abbreviation': 0.50, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Cntrl', 'CENTRAL', i_round=2)._asdict())
        assert x == {'abbreviation': 0.50, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Cntrl', 'centrale', i_round=2)._asdict())
        assert x == {'abbreviation': 0.33, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Cntrl', 'CONTRAIL', i_round=2)._asdict())
        assert x == {'abbreviation': 0.33, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Cntrl', 'Control', i_round=2)._asdict())
        assert x == {'abbreviation': 0.50, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Cntrl', 'CONTROLE', i_round=2)._asdict())
        assert x == {'abbreviation': 0.33, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Cntrl', 'CONTROUL', i_round=2)._asdict())
        assert x == {'abbreviation': 0.33, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Cntrl', 'COUNTROL', i_round=2)._asdict())
        assert x == {'abbreviation': 0.33, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Composit', 'Composite', i_round=2)._asdict())
        assert x == {'abbreviation': 0.06, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Continous', 'Continuous', i_round=2)._asdict())
        assert x == {'abbreviation': 0.04, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Cotz', 'coetzee', i_round=2)._asdict())
        assert x == {'abbreviation': 0.19, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Cupler', 'Coupler', i_round=2)._asdict())
        assert x == {'abbreviation': 0.11, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Cupler', 'CUPELER', i_round=2)._asdict())
        assert x == {'abbreviation': 0.11, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Dble', 'DEBILE', i_round=2)._asdict())
        assert x == {'abbreviation': 0.22, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Dble', 'DEIBLE', i_round=2)._asdict())
        assert x == {'abbreviation': 0.22, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Dble', 'DIABLE', i_round=2)._asdict())
        assert x == {'abbreviation': 0.22, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Dble', 'DIBLE', i_round=2)._asdict())
        assert x == {'abbreviation': 0.25, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Dble', 'DIBLEY', i_round=2)._asdict())
        assert x == {'abbreviation': 0.06, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Dble', 'DOABLE', i_round=2)._asdict())
        assert x == {'abbreviation': 0.22, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Dble', 'DOBLE', i_round=2)._asdict())
        assert x == {'abbreviation': 0.25, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Dble', 'Double', i_round=2)._asdict())
        assert x == {'abbreviation': 0.22, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Dble', 'DUBLE', i_round=2)._asdict())
        assert x == {'abbreviation': 0.25, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Dble', 'DYABLE', i_round=2)._asdict())
        assert x == {'abbreviation': 0.06, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Dble', 'DYEABLE', i_round=2)._asdict())
        assert x == {'abbreviation': 0.08, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Deadend', 'DEADENED', i_round=2)._asdict())
        assert x == {'abbreviation': 0.06, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Delux', 'Deluxe', i_round=2)._asdict())
        assert x == {'abbreviation': 0.11, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Fastners', 'Fasteners', i_round=2)._asdict())
        assert x == {'abbreviation': 0.11, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Fluro', 'fluoro', i_round=2)._asdict())
        assert x == {'abbreviation': 0.11, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Frac', 'FARACE', i_round=2)._asdict())
        assert x == {'abbreviation': 0.22, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Frac', 'FARACI', i_round=2)._asdict())
        assert x == {'abbreviation': 0.22, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Frac', 'FARACO', i_round=2)._asdict())
        assert x == {'abbreviation': 0.22, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Frac', 'FERACO', i_round=2)._asdict())
        assert x == {'abbreviation': 0.22, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Fsteners', 'Fasteners', i_round=2)._asdict())
        assert x == {'abbreviation': 0.11, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Goosneck', 'Gooseneck', i_round=2)._asdict())
        assert x == {'abbreviation': 0.04, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Goseneck', 'Gooseneck', i_round=2)._asdict())
        assert x == {'abbreviation': 0.04, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Gudg', 'GAUDGIE', i_round=2)._asdict())
        assert x == {'abbreviation': 0.19, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Gudg', 'GOUDGE', i_round=2)._asdict())
        assert x == {'abbreviation': 0.22, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Gudg', 'GUIDAGE', i_round=2)._asdict())
        assert x == {'abbreviation': 0.19, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Hatchs', 'Hatches', i_round=2)._asdict())
        assert x == {'abbreviation': 0.11, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Jackline', 'JACKELINE', i_round=2)._asdict())
        assert x == {'abbreviation': 0.04, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('lgth', 'LEGATH', i_round=2)._asdict())
        assert x == {'abbreviation': 0.22, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Lngth', 'Length', i_round=2)._asdict())
        assert x == {'abbreviation': 0.25, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Lngth', 'LENGTHY', i_round=2)._asdict())
        assert x == {'abbreviation': 0.06, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Lume', 'LAUMEA', i_round=2)._asdict())
        assert x == {'abbreviation': 0.12, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Matic', 'MAIEUTIC', i_round=2)._asdict())
        assert x == {'abbreviation': 0.12, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Matic', 'matica', i_round=2)._asdict())
        assert x == {'abbreviation': 0.11, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Matic', 'MATICE', i_round=2)._asdict())
        assert x == {'abbreviation': 0.11, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Matic', 'MATICO', i_round=2)._asdict())
        assert x == {'abbreviation': 0.11, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('pigt', 'piaget', i_round=2)._asdict())
        assert x == {'abbreviation': 0.22, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('pigt', 'PIGAT', i_round=2)._asdict())
        assert x == {'abbreviation': 0.25, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('pigt', 'pigot', i_round=2)._asdict())
        assert x == {'abbreviation': 0.25, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('pigt', 'PIGOUT', i_round=2)._asdict())
        assert x == {'abbreviation': 0.22, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('pigt', 'PIGUET', i_round=2)._asdict())
        assert x == {'abbreviation': 0.22, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Pinst', 'PIANIST', i_round=2)._asdict())
        assert x == {'abbreviation': 0.06, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 1.0}
        x = dict(Probabilities('Pinst', 'PIANISTE', i_round=2)._asdict())
        assert x == {'abbreviation': 0.08, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 1.0}
        x = dict(Probabilities('Pinst', 'PINIEST', i_round=2)._asdict())
        assert x == {'abbreviation': 0.22, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Plgr', 'peligro', i_round=2)._asdict())
        assert x == {'abbreviation': 0.33, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Plgr', 'Pilger', i_round=2)._asdict())
        assert x == {'abbreviation': 0.50, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Plgr', 'PLAGUER', i_round=2)._asdict())
        assert x == {'abbreviation': 0.33, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Plgr', 'PLOEGER', i_round=2)._asdict())
        assert x == {'abbreviation': 0.33, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Plgr', 'PLUEGER', i_round=2)._asdict())
        assert x == {'abbreviation': 0.33, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Plgr', 'polgar', i_round=2)._asdict())
        assert x == {'abbreviation': 0.50, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Polypropylen', 'Polypropylene', i_round=2)._asdict())
        assert x == {'abbreviation': 0.03, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
'''
'''
        x = dict(Probabilities('Remov', 'REMOVE', i_round=2)._asdict())
        assert x == {'abbreviation': 0.11, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Replac', 'Replace', i_round=2)._asdict())
        assert x == {'abbreviation': 0.11, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Replacment', 'Replacement', i_round=2)._asdict())
        assert x == {'abbreviation': 0.06, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Replc', 'Replace', i_round=2)._asdict())
        assert x == {'abbreviation': 0.22, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Replc', 'REPLICA', i_round=2)._asdict())
        assert x == {'abbreviation': 0.22, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Rodk', 'RODAK', i_round=2)._asdict())
        assert x == {'abbreviation': 0.25, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Rodk', 'RODKEY', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 1.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Salti', 'SALTIE', i_round=2)._asdict())
        assert x == {'abbreviation': 0.11, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Senstive', 'Sensitive', i_round=2)._asdict())
        assert x == {'abbreviation': 0.06, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Sgls', 'SEGOLS', i_round=2)._asdict())
        assert x == {'abbreviation': 0.50, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Sgls', 'SIGILS', i_round=2)._asdict())
        assert x == {'abbreviation': 0.50, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Sgls', 'SIGLAS', i_round=2)._asdict())
        assert x == {'abbreviation': 0.50, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Sgls', 'SIGLOS', i_round=2)._asdict())
        assert x == {'abbreviation': 0.50, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Sheav', 'Sheave', i_round=2)._asdict())
        assert x == {'abbreviation': 0.06, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('sheve', 'Sheave', i_round=2)._asdict())
        assert x == {'abbreviation': 0.06, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('sheve', 'SHEEVE', i_round=2)._asdict())
        assert x == {'abbreviation': 0.06, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('sheve', 'SHEIVE', i_round=2)._asdict())
        assert x == {'abbreviation': 0.06, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Shorti', 'SHORTIA', i_round=2)._asdict())
        assert x == {'abbreviation': 0.06, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Shorti', 'Shortie', i_round=2)._asdict())
        assert x == {'abbreviation': 0.06, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Shve', 'SHAVE', i_round=2)._asdict())
        assert x == {'abbreviation': 0.11, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Shve', 'SHAVIE', i_round=2)._asdict())
        assert x == {'abbreviation': 0.12, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Shve', 'Sheave', i_round=2)._asdict())
        assert x == {'abbreviation': 0.12, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Shve', 'SHEEVE', i_round=2)._asdict())
        assert x == {'abbreviation': 0.12, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Shve', 'SHEIVE', i_round=2)._asdict())
        assert x == {'abbreviation': 0.12, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Shve', 'SHIVE', i_round=2)._asdict())
        assert x == {'abbreviation': 0.11, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Shve', 'SHOVE', i_round=2)._asdict())
        assert x == {'abbreviation': 0.11, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Shve', 'SHVEY', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': -1.0}
        x = dict(Probabilities('Sngl', 'sangala', i_round=2)._asdict())
        assert x == {'abbreviation': 0.33, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Sngl', 'sangley', i_round=2)._asdict())
        assert x == {'abbreviation': 0.11, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Sngl', 'sangli', i_round=2)._asdict())
        assert x == {'abbreviation': 0.50, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Sngl', 'Senegal', i_round=2)._asdict())
        assert x == {'abbreviation': 0.33, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Sngl', 'SINEGAL', i_round=2)._asdict())
        assert x == {'abbreviation': 0.33, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Sngl', 'Singl', i_round=2)._asdict())
        assert x == {'abbreviation': 1.00, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Sngl', 'Single', i_round=2)._asdict())
        assert x == {'abbreviation': 0.50, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Sngl', 'SINGLEY', i_round=2)._asdict())
        assert x == {'abbreviation': 0.11, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Sngl', 'SINGLY', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Sngl', 'SNUGLY', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Stanless', 'Stainless', i_round=2)._asdict())
        assert x == {'abbreviation': 0.06, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Strght', 'Straight', i_round=2)._asdict())
        assert x == {'abbreviation': 0.50, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Strght', 'STRAUGHT', i_round=2)._asdict())
        assert x == {'abbreviation': 0.50, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Strght', 'STREIGHT', i_round=2)._asdict())
        assert x == {'abbreviation': 0.50, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Strght', 'STRIGHT', i_round=2)._asdict())
        assert x == {'abbreviation': 1.00, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Swvl', 'SWAVELY', i_round=2)._asdict())
        assert x == {'abbreviation': 0.11, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Swvl', 'Swivel', i_round=2)._asdict())
        assert x == {'abbreviation': 0.50, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Telo', 'TELOI', i_round=2)._asdict())
        assert x == {'abbreviation': 0.11, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Towabl', 'Towable', i_round=2)._asdict())
        assert x == {'abbreviation': 0.11, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Trav', 'TRAVE', i_round=2)._asdict())
        assert x == {'abbreviation': 0.25, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Trilite', 'TROILITE', i_round=2)._asdict())
        assert x == {'abbreviation': 0.06, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Twing', 'TAWING', i_round=2)._asdict())
        assert x == {'abbreviation': 0.25, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Twing', 'TEWING', i_round=2)._asdict())
        assert x == {'abbreviation': 0.25, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Twing', 'Towing', i_round=2)._asdict())
        assert x == {'abbreviation': 0.25, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Twing', 'TWINGE', i_round=2)._asdict())
        assert x == {'abbreviation': 0.25, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Univers', 'UNIVERSE', i_round=2)._asdict())
        assert x == {'abbreviation': 0.11, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Univers', 'universo', i_round=2)._asdict())
        assert x == {'abbreviation': 0.11, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Upstd', 'UPSTAYED', i_round=2)._asdict())
        assert x == {'abbreviation': 0.0, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Upstd', 'UPSTOOD', i_round=2)._asdict())
        assert x == {'abbreviation': 0.50, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
        x = dict(Probabilities('Upstnd', 'Upstand', i_round=2)._asdict())
        assert x == {'abbreviation': 1.00, 'plural': 0.0, 'singular': 0.0, 'truncation': 0.0, 'tense': 0.0, 'typo': 0.0}
'''

'''
class TestEdits():
    def test_edits(self):
        results = edits('Vneck', 'VANEYCK')
        assert results == {'removed': ['a', 'y'], 'changed': [], 'added': []}
        results = edits('Summner', 'SUMMONER')
        assert results == {'removed': ['o'], 'changed': [], 'added': []}
        results = edits('Summner', 'SUMMER')
        assert results == {'removed': [], 'changed': [], 'added': ['n']}
        results = edits('Slvless', 'Sleeveless')
        assert results == {'removed': ['e', 'e', 'e'], 'changed': [], 'added': []}
        results = edits('Howllow', 'Hollow')
        assert results == {'removed': [], 'changed': [], 'added': ['w']}
        results = edits('Gastby', 'Gatsby')
        assert results == {'removed': ['t'], 'changed': [], 'added': ['t']}
        results = edits('Classicial', 'Classical')
        assert results == {'removed': [], 'changed': [], 'added': ['i']}
        results = edits('VOGTAGE', 'VOLTAGE')
        assert results == {'removed': [], 'changed': [('g', 'l')], 'added': []}
        results = edits('Daallas', 'Dallas')
        assert results == {'removed': [], 'changed': [], 'added': ['a']}
        results = edits('Dallas', 'Dalas')
        assert results == {'removed': [], 'changed': [], 'added': ['l']}
        results = edits('Mist', 'Nist')
        assert results == {'removed': [], 'changed': [('m', 'n')], 'added': []}
        results = edits('Shenendoah', 'Shenandoah')
        assert results == {'removed': [], 'changed': [('e', 'a')], 'added': []}
        results = edits('Med', 'Medium')
        assert results == {'removed': ['i', 'u', 'm'], 'changed': [], 'added': []}
        results = edits('Shenendoa', 'Shenandoah')
        assert results == {'removed': ['h'], 'changed': [('e', 'a')], 'added': []}
        results = edits('Shenendoah', 'Shenantoah')
        assert results == {'removed': [], 'changed': [('e', 'a'), ('d', 't')], 'added': []}
        results = edits('Mellon', 'Melo')
        assert results == {'removed': [], 'changed': [], 'added': ['l', 'n']}
        results = edits('Blc', 'Black')
        assert results == {'removed': ['a', 'k'], 'changed': [], 'added': []}
        results = edits('cat', 'vat')
        assert results == {'removed': [], 'changed': [('c', 'v')], 'added': []}
        results = edits('cat', 'cats')
        assert results == {'removed': ['s'], 'changed': [], 'added': []}
        results = edits('books', 'book')
        assert results == {'removed': [], 'changed': [], 'added': ['s']}

        results = edits('Acetel', 'Acetal')
        assert results == {'removed': [], 'changed': [('e', 'a')], 'added': []}
        results = edits('Ajduster', 'Adjuster')
        assert results == {'removed': ['d'], 'changed': [], 'added': ['d']}
        results = edits('Altantic', 'Atlantic')
        assert results == {'removed': ['t'], 'changed': [], 'added': ['t']}
        results = edits('Aqualon', 'Aqualyn')
        assert results == {'removed': [], 'changed': [('o', 'y')], 'added': []}
        results = edits('Beckect', 'Becket')
        assert results == {'removed': [], 'changed': [], 'added': ['c']}
        results = edits('Bouyancy', 'Buoyancy')
        assert results == {'removed': ['u'], 'changed': [], 'added': ['u']}
        results = edits('Braket', 'Brake')
        assert results == {'removed': [], 'changed': [], 'added': ['t']}
        results = edits('Crugear', 'Cruger')
        assert results == {'removed': [], 'changed': [], 'added': ['a']}
        results = edits('Cupler', 'Cuyler')
        assert results == {'removed': [], 'changed': [('p', 'y')], 'added': []}
        results = edits('Dufflel', 'Duffel')
        assert results == {'removed': [], 'changed': [], 'added': ['l']}
        results = edits('Dufflel', 'Duffle')
        assert results == {'removed': [], 'changed': [], 'added': ['l']}
        results = edits('Elastromer', 'Elastomer')
        assert results == {'removed': [], 'changed': [], 'added': ['r']}
        results = edits('Firfely', 'Firefly')
        assert results == {'removed': ['e'], 'changed': [], 'added': ['e']}
        results = edits('Flairlead', 'Fairlead')
        assert results == {'removed': [], 'changed': [], 'added': ['l']}
        results = edits('Furlex', 'Furled')
        assert results == {'removed': [], 'changed': [('x', 'd')], 'added': []}
        results = edits('Furlex', 'Furler')
        assert results == {'removed': [], 'changed': [('x', 'r')], 'added': []}
        results = edits('Gaurds', 'Guards')
        assert results == {'removed': ['u'], 'changed': [], 'added': ['u']}
        results = edits('Hatchs', 'Hatch')
        assert results == {'removed': [], 'changed': [], 'added': ['s']}
        results = edits('Helmsmans', 'Helmsman')
        assert results == {'removed': [], 'changed': [], 'added': ['s']}
        results = edits('Houseing', 'Housing')
        assert results == {'removed': [], 'changed': [], 'added': ['e']}
        results = edits('Kanvas', 'Kansas')
        assert results == {'removed': [], 'changed': [('v', 's')], 'added': []}
        results = edits('Kanvas', 'kanva')
        assert results == {'removed': [], 'changed': [], 'added': ['s']}
        results = edits('LaserA', 'Laser')
        assert results == {'removed': [], 'changed': [], 'added': ['a']}
        results = edits('LaserA', 'Lasers')
        assert results == {'removed': [], 'changed': [('a', 's')], 'added': []}
        results = edits('Lloyed', 'Lloyd')
        assert results == {'removed': [], 'changed': [], 'added': ['e']}
        results = edits('Melges', 'meles')
        assert results == {'removed': [], 'changed': [], 'added': ['g']}
        results = edits('Mohogany', 'Mahogany')
        assert results == {'removed': [], 'changed': [('o', 'a')], 'added': []}
        results = edits('Nylone', 'Nylon')
        assert results == {'removed': [], 'changed': [], 'added': ['e']}
        results = edits('Pinstop', 'pitstop')
        assert results == {'removed': [], 'changed': [('n', 't')], 'added': []}
        results = edits('Pivoiting', 'Pivoting')
        assert results == {'removed': [], 'changed': [], 'added': ['i']}
        results = edits('Protex', 'Propex')
        assert results == {'removed': [], 'changed': [('t', 'p')], 'added': []}
        results = edits('Protex', 'Protem')
        assert results == {'removed': [], 'changed': [('x', 'm')], 'added': []}
        results = edits('Sahckle', 'Shackle')
        assert results == {'removed': ['h'], 'changed': [], 'added': ['h']}
        results = edits('Serier', 'Series')
        assert results == {'removed': [], 'changed': [('r', 's')], 'added': []}
        results = edits('Serires', 'Series')
        assert results == {'removed': [], 'changed': [], 'added': ['r']}
        results = edits('Sharki', 'shaki')
        assert results == {'removed': [], 'changed': [], 'added': ['r']}
        results = edits('Sharki', 'Shari')
        assert results == {'removed': [], 'changed': [], 'added': ['k']}
        results = edits('Sharki', 'Shark')
        assert results == {'removed': [], 'changed': [], 'added': ['i']}
        results = edits('Sharktooth', 'Sharptooth')
        assert results == {'removed': [], 'changed': [('k', 'p')], 'added': []}
        results = edits('Shorti', 'Short')
        assert results == {'removed': [], 'changed': [], 'added': ['i']}
        results = edits('Shorti', 'Shorts')
        assert results == {'removed': [], 'changed': [('i', 's')], 'added': []}
        results = edits('Sparcraft', 'starcraft')
        assert results == {'removed': [], 'changed': [('p', 't')], 'added': []}
        results = edits('Sprial', 'Spiral')
        assert results == {'removed': ['i'], 'changed': [], 'added': ['i']}
        results = edits('Stainloess', 'Stainless')
        assert results == {'removed': [], 'changed': [], 'added': ['o']}
        results = edits('Stanless', 'Stanleys')
        assert results == {'removed': [], 'changed': [('s', 'y')], 'added': []}
        results = edits('Sumbrella', 'Sunbrella')
        assert results == {'removed': [], 'changed': [('m', 'n')], 'added': []}
        results = edits('SunfishA', 'Sunfish')
        assert results == {'removed': [], 'changed': [], 'added': ['a']}
        results = edits('Tenion', 'Tenino')
        assert results == {'removed': ['n'], 'changed': [], 'added': ['n']}
        results = edits('Tenion', 'Tenon')
        assert results == {'removed': [], 'changed': [], 'added': ['i']}
        results = edits('Thmible', 'Thimble')
        assert results == {'removed': ['i'], 'changed': [], 'added': ['i']}
        results = edits('Threead', 'Thread')
        assert results == {'removed': [], 'changed': [], 'added': ['e']}
        results = edits('Torlon', 'Toulon')
        assert results == {'removed': [], 'changed': [('r', 'u')], 'added': []}
        results = edits('Trackw', 'Track')
        assert results == {'removed': [], 'changed': [], 'added': ['w']}
        results = edits('Trailex', 'Trailer')
        assert results == {'removed': [], 'changed': [('x', 'r')], 'added': []}
        results = edits('Tutnbuckle', 'Turnbuckle')
        assert results == {'removed': [], 'changed': [('t', 'r')], 'added': []}
        results = edits('Univeresal', 'Universal')
        assert results == {'removed': [], 'changed': [], 'added': ['e']}
        results = edits('Windshift', 'Windshirt')
        assert results == {'removed': [], 'changed': [('f', 'r')], 'added': []}

        results = edits('Anod', 'ANODE')
        assert results == {'removed': ['e'], 'changed': [], 'added': []}
        results = edits('Anodz', 'ANODIZE')
        assert results == {'removed': ['i', 'e'], 'changed': [], 'added': []}
        results = edits('Assy', 'ASSAY')
        assert results == {'removed': ['a'], 'changed': [], 'added': []}
        results = edits('Assy', 'assaye')
        assert results == {'removed': ['a', 'e'], 'changed': [], 'added': []}
        results = edits('Beckt', 'Becket')
        assert results == {'removed': ['e'], 'changed': [], 'added': []}
        results = edits('Brait', 'baraita')
        assert results == {'removed': ['a', 'a'], 'changed': [], 'added': []}
        results = edits('Brckt', 'Bracket')
        assert results == {'removed': ['a', 'e'], 'changed': [], 'added': []}
        results = edits('Brckt', 'BROCKET')
        assert results == {'removed': ['o', 'e'], 'changed': [], 'added': []}
        results = edits('Brckt', 'BROCKIT')
        assert results == {'removed': ['o', 'i'], 'changed': [], 'added': []}
        results = edits('Brkt', 'BARAKAT')
        assert results == {'removed': ['a', 'a', 'a'], 'changed': [], 'added': []}
        results = edits('Brkt', 'barkat')
        assert results == {'removed': ['a', 'a'], 'changed': [], 'added': []}
        results = edits('Brkt', 'BARKET')
        assert results == {'removed': ['a', 'e'], 'changed': [], 'added': []}
        results = edits('Brkt', 'Burket')
        assert results == {'removed': ['u', 'e'], 'changed': [], 'added': []}
        results = edits('Brkt', 'BURKITE')
        assert results == {'removed': ['u', 'i', 'e'], 'changed': [], 'added': []}
        results = edits('Cluch', 'CELUCH')
        assert results == {'removed': ['e'], 'changed': [], 'added': []}
        results = edits('Cluch', 'CLEUCH')
        assert results == {'removed': ['e'], 'changed': [], 'added': []}
        results = edits('Cntr', 'CANTAR')
        assert results == {'removed': ['a', 'a'], 'changed': [], 'added': []}
        results = edits('Cntr', 'CANTARA')
        assert results == {'removed': ['a', 'a', 'a'], 'changed': [], 'added': []}
        results = edits('Cntr', 'CANTER')
        assert results == {'removed': ['a', 'e'], 'changed': [], 'added': []}
        results = edits('Cntr', 'CANTERO')
        assert results == {'removed': ['a', 'e', 'o'], 'changed': [], 'added': []}
        results = edits('Cntr', 'CANTIER')
        assert results == {'removed': ['a', 'i', 'e'], 'changed': [], 'added': []}
        results = edits('Cntr', 'CANTOR')
        assert results == {'removed': ['a', 'o'], 'changed': [], 'added': []}
        results = edits('Cntr', 'CANTORE')
        assert results == {'removed': ['a', 'o', 'e'], 'changed': [], 'added': []}
        results = edits('Cntr', 'CANTRE')
        assert results == {'removed': ['a', 'e'], 'changed': [], 'added': []}
        results = edits('Cntr', 'CANTRIO')
        assert results == {'removed': ['a', 'i', 'o'], 'changed': [], 'added': []}
        results = edits('Cntr', 'CENTARE')
        assert results == {'removed': ['e', 'a', 'e'], 'changed': [], 'added': []}
        results = edits('Cntr', 'CENTAUR')
        assert results == {'removed': ['e', 'a', 'u'], 'changed': [], 'added': []}
        results = edits('Cntr', 'Center')
        assert results == {'removed': ['e', 'e'], 'changed': [], 'added': []}
        results = edits('Cntr', 'CENTORE')
        assert results == {'removed': ['e', 'o', 'e'], 'changed': [], 'added': []}
        results = edits('Cntr', 'CENTRA')
        assert results == {'removed': ['e', 'a'], 'changed': [], 'added': []}
        results = edits('Cntr', 'CENTRE')
        assert results == {'removed': ['e', 'e'], 'changed': [], 'added': []}
        results = edits('Cntr', 'CENTRY')
        assert results == {'removed': ['e', 'y'], 'changed': [], 'added': []}
        results = edits('Cntr', 'Centura')
        assert results == {'removed': ['e', 'u', 'a'], 'changed': [], 'added': []}
        results = edits('Cntr', 'CENTURY')
        assert results == {'removed': ['e', 'u', 'y'], 'changed': [], 'added': []}
        results = edits('Cntr', 'CINTORA')
        assert results == {'removed': ['i', 'o', 'a'], 'changed': [], 'added': []}
        results = edits('Cntr', 'CINTRA')
        assert results == {'removed': ['i', 'a'], 'changed': [], 'added': []}
        results = edits('Cntr', 'COINTER')
        assert results == {'removed': ['o', 'i', 'e'], 'changed': [], 'added': []}
        results = edits('Cntr', 'CONTOUR')
        assert results == {'removed': ['o', 'o', 'u'], 'changed': [], 'added': []}
        results = edits('Cntr', 'contr')
        assert results == {'removed': ['o'], 'changed': [], 'added': []}
        results = edits('Cntr', 'CONTRA')
        assert results == {'removed': ['o', 'a'], 'changed': [], 'added': []}
        results = edits('Cntr', 'contre')
        assert results == {'removed': ['o', 'e'], 'changed': [], 'added': []}
        results = edits('Cntr', 'contro')
        assert results == {'removed': ['o', 'o'], 'changed': [], 'added': []}
        results = edits('Cntr', 'COUNTER')
        assert results == {'removed': ['o', 'u', 'e'], 'changed': [], 'added': []}
        results = edits('Cntr', 'COUNTRY')
        assert results == {'removed': ['o', 'u', 'y'], 'changed': [], 'added': []}
        results = edits('Cntrl', 'CANTORAL')
        assert results == {'removed': ['a', 'o', 'a'], 'changed': [], 'added': []}
        results = edits('Cntrl', 'CANTREL')
        assert results == {'removed': ['a', 'e'], 'changed': [], 'added': []}
        results = edits('Cntrl', 'Cantril')
        assert results == {'removed': ['a', 'i'], 'changed': [], 'added': []}
        results = edits('Cntrl', 'CENTRAL')
        assert results == {'removed': ['e', 'a'], 'changed': [], 'added': []}
        results = edits('Cntrl', 'centrale')
        assert results == {'removed': ['e', 'a', 'e'], 'changed': [], 'added': []}
        results = edits('Cntrl', 'CONTRAIL')
        assert results == {'removed': ['o', 'a', 'i'], 'changed': [], 'added': []}
        results = edits('Cntrl', 'Control')
        assert results == {'removed': ['o', 'o'], 'changed': [], 'added': []}
        results = edits('Cntrl', 'CONTROLE')
        assert results == {'removed': ['o', 'o', 'e'], 'changed': [], 'added': []}
        results = edits('Cntrl', 'CONTROUL')
        assert results == {'removed': ['o', 'o', 'u'], 'changed': [], 'added': []}
        results = edits('Cntrl', 'COUNTROL')
        assert results == {'removed': ['o', 'u', 'o'], 'changed': [], 'added': []}
        results = edits('Composit', 'Composite')
        assert results == {'removed': ['e'], 'changed': [], 'added': []}
        results = edits('Continous', 'Continuous')
        assert results == {'removed': ['u'], 'changed': [], 'added': []}
        results = edits('Cotz', 'coetzee')
        assert results == {'removed': ['e', 'e', 'e'], 'changed': [], 'added': []}
        results = edits('Cupler', 'Coupler')
        assert results == {'removed': ['o'], 'changed': [], 'added': []}
        results = edits('Cupler', 'CUPELER')
        assert results == {'removed': ['e'], 'changed': [], 'added': []}
        results = edits('Dble', 'DEBILE')
        assert results == {'removed': ['e', 'i'], 'changed': [], 'added': []}
        results = edits('Dble', 'DEIBLE')
        assert results == {'removed': ['e', 'i'], 'changed': [], 'added': []}
        results = edits('Dble', 'DIABLE')
        assert results == {'removed': ['i', 'a'], 'changed': [], 'added': []}
        results = edits('Dble', 'DIBLE')
        assert results == {'removed': ['i'], 'changed': [], 'added': []}
        results = edits('Dble', 'DIBLEY')
        assert results == {'removed': ['i', 'y'], 'changed': [], 'added': []}
        results = edits('Dble', 'DOABLE')
        assert results == {'removed': ['o', 'a'], 'changed': [], 'added': []}
        results = edits('Dble', 'DOBLE')
        assert results == {'removed': ['o'], 'changed': [], 'added': []}
        results = edits('Dble', 'Double')
        assert results == {'removed': ['o', 'u'], 'changed': [], 'added': []}
        results = edits('Dble', 'DUBLE')
        assert results == {'removed': ['u'], 'changed': [], 'added': []}
        results = edits('Dble', 'DYABLE')
        assert results == {'removed': ['y', 'a'], 'changed': [], 'added': []}
        results = edits('Dble', 'DYEABLE')
        assert results == {'removed': ['y', 'e', 'a'], 'changed': [], 'added': []}
        results = edits('Deadend', 'DEADENED')
        assert results == {'removed': ['e'], 'changed': [], 'added': []}
        results = edits('Delux', 'Deluxe')
        assert results == {'removed': ['e'], 'changed': [], 'added': []}
        results = edits('Fastners', 'Fasteners')
        assert results == {'removed': ['e'], 'changed': [], 'added': []}
        results = edits('Fluro', 'fluoro')
        assert results == {'removed': ['o'], 'changed': [], 'added': []}
        results = edits('Frac', 'FARACE')
        assert results == {'removed': ['a', 'e'], 'changed': [], 'added': []}
        results = edits('Frac', 'FARACI')
        assert results == {'removed': ['a', 'i'], 'changed': [], 'added': []}
        results = edits('Frac', 'FARACO')
        assert results == {'removed': ['a', 'o'], 'changed': [], 'added': []}
        results = edits('Frac', 'FERACO')
        assert results == {'removed': ['e', 'o'], 'changed': [], 'added': []}
        results = edits('Fsteners', 'Fasteners')
        assert results == {'removed': ['a'], 'changed': [], 'added': []}
        results = edits('Goosneck', 'Gooseneck')
        assert results == {'removed': ['e'], 'changed': [], 'added': []}
        results = edits('Goseneck', 'Gooseneck')
        assert results == {'removed': ['o'], 'changed': [], 'added': []}
        results = edits('Gudg', 'GAUDGIE')
        assert results == {'removed': ['a', 'i', 'e'], 'changed': [], 'added': []}
        results = edits('Gudg', 'GOUDGE')
        assert results == {'removed': ['o', 'e'], 'changed': [], 'added': []}
        results = edits('Gudg', 'GUIDAGE')
        assert results == {'removed': ['i', 'a', 'e'], 'changed': [], 'added': []}
        results = edits('Hatchs', 'Hatches')
        assert results == {'removed': ['e'], 'changed': [], 'added': []}
        results = edits('Jackline', 'JACKELINE')
        assert results == {'removed': ['e'], 'changed': [], 'added': []}
        results = edits('lgth', 'LEGATH')
        assert results == {'removed': ['e', 'a'], 'changed': [], 'added': []}
        results = edits('Lngth', 'Length')
        assert results == {'removed': ['e'], 'changed': [], 'added': []}
        results = edits('Lngth', 'LENGTHY')
        assert results == {'removed': ['e', 'y'], 'changed': [], 'added': []}
        results = edits('Lume', 'LAUMEA')
        assert results == {'removed': ['a', 'a'], 'changed': [], 'added': []}
        results = edits('Matic', 'MAIEUTIC')
        assert results == {'removed': ['i', 'e', 'u'], 'changed': [], 'added': []}
        results = edits('Matic', 'matica')
        assert results == {'removed': ['a'], 'changed': [], 'added': []}
        results = edits('Matic', 'MATICE')
        assert results == {'removed': ['e'], 'changed': [], 'added': []}
        results = edits('Matic', 'MATICO')
        assert results == {'removed': ['o'], 'changed': [], 'added': []}
        results = edits('pigt', 'piaget')
        assert results == {'removed': ['a', 'e'], 'changed': [], 'added': []}
        results = edits('pigt', 'PIGAT')
        assert results == {'removed': ['a'], 'changed': [], 'added': []}
        results = edits('pigt', 'pigot')
        assert results == {'removed': ['o'], 'changed': [], 'added': []}
        results = edits('pigt', 'PIGOUT')
        assert results == {'removed': ['o', 'u'], 'changed': [], 'added': []}
        results = edits('pigt', 'PIGUET')
        assert results == {'removed': ['u', 'e'], 'changed': [], 'added': []}
        results = edits('Pinst', 'PIANIST')
        assert results == {'removed': ['a', 'i'], 'changed': [], 'added': []}
        results = edits('Pinst', 'PIANISTE')
        assert results == {'removed': ['a', 'i', 'e'], 'changed': [], 'added': []}
        results = edits('Pinst', 'PINIEST')
        assert results == {'removed': ['i', 'e'], 'changed': [], 'added': []}
        results = edits('Plgr', 'peligro')
        assert results == {'removed': ['e', 'i', 'o'], 'changed': [], 'added': []}
        results = edits('Plgr', 'Pilger')
        assert results == {'removed': ['i', 'e'], 'changed': [], 'added': []}
        results = edits('Plgr', 'PLAGUER')
        assert results == {'removed': ['a', 'u', 'e'], 'changed': [], 'added': []}
        results = edits('Plgr', 'PLOEGER')
        assert results == {'removed': ['o', 'e', 'e'], 'changed': [], 'added': []}
        results = edits('Plgr', 'PLUEGER')
        assert results == {'removed': ['u', 'e', 'e'], 'changed': [], 'added': []}
        results = edits('Plgr', 'polgar')
        assert results == {'removed': ['o', 'a'], 'changed': [], 'added': []}
        results = edits('Polypropylen', 'Polypropylene')
        assert results == {'removed': ['e'], 'changed': [], 'added': []}
        results = edits('Remov', 'REMOVE')
        assert results == {'removed': ['e'], 'changed': [], 'added': []}
        results = edits('Replac', 'Replace')
        assert results == {'removed': ['e'], 'changed': [], 'added': []}
        results = edits('Replacment', 'Replacement')
        assert results == {'removed': ['e'], 'changed': [], 'added': []}
        results = edits('Replc', 'Replace')
        assert results == {'removed': ['a', 'e'], 'changed': [], 'added': []}
        results = edits('Replc', 'REPLICA')
        assert results == {'removed': ['i', 'a'], 'changed': [], 'added': []}
        results = edits('Rodk', 'RODAK')
        assert results == {'removed': ['a'], 'changed': [], 'added': []}
        results = edits('Rodk', 'RODKEY')
        assert results == {'removed': ['e', 'y'], 'changed': [], 'added': []}
        results = edits('Salti', 'SALTIE')
        assert results == {'removed': ['e'], 'changed': [], 'added': []}
        results = edits('Senstive', 'Sensitive')
        assert results == {'removed': ['i'], 'changed': [], 'added': []}
        results = edits('Sgls', 'SEGOLS')
        assert results == {'removed': ['e', 'o'], 'changed': [], 'added': []}
        results = edits('Sgls', 'SIGILS')
        assert results == {'removed': ['i', 'i'], 'changed': [], 'added': []}
        results = edits('Sgls', 'SIGLAS')
        assert results == {'removed': ['i', 'a'], 'changed': [], 'added': []}
        results = edits('Sgls', 'SIGLOS')
        assert results == {'removed': ['i', 'o'], 'changed': [], 'added': []}
        results = edits('Sheav', 'Sheave')
        assert results == {'removed': ['e'], 'changed': [], 'added': []}
        results = edits('sheve', 'Sheave')
        assert results == {'removed': ['a'], 'changed': [], 'added': []}
        results = edits('sheve', 'SHEEVE')
        assert results == {'removed': ['e'], 'changed': [], 'added': []}
        results = edits('sheve', 'SHEIVE')
        assert results == {'removed': ['i'], 'changed': [], 'added': []}
        results = edits('Shorti', 'SHORTIA')
        assert results == {'removed': ['a'], 'changed': [], 'added': []}
        results = edits('Shorti', 'Shortie')
        assert results == {'removed': ['e'], 'changed': [], 'added': []}
        results = edits('Shve', 'SHAVE')
        assert results == {'removed': ['a'], 'changed': [], 'added': []}
        results = edits('Shve', 'SHAVIE')
        assert results == {'removed': ['a', 'i'], 'changed': [], 'added': []}
        results = edits('Shve', 'Sheave')
        assert results == {'removed': ['e', 'a'], 'changed': [], 'added': []}
        results = edits('Shve', 'SHEEVE')
        assert results == {'removed': ['e', 'e'], 'changed': [], 'added': []}
        results = edits('Shve', 'SHEIVE')
        assert results == {'removed': ['e', 'i'], 'changed': [], 'added': []}
        results = edits('Shve', 'SHIVE')
        assert results == {'removed': ['i'], 'changed': [], 'added': []}
        results = edits('Shve', 'SHOVE')
        assert results == {'removed': ['o'], 'changed': [], 'added': []}
        results = edits('Shve', 'SHVEY')
        assert results == {'removed': ['y'], 'changed': [], 'added': []}
        results = edits('Sngl', 'sangala')
        assert results == {'removed': ['a', 'a', 'a'], 'changed': [], 'added': []}
        results = edits('Sngl', 'sangley')
        assert results == {'removed': ['a', 'e', 'y'], 'changed': [], 'added': []}
        results = edits('Sngl', 'sangli')
        assert results == {'removed': ['a', 'i'], 'changed': [], 'added': []}
        results = edits('Sngl', 'Senegal')
        assert results == {'removed': ['e', 'e', 'a'], 'changed': [], 'added': []}
        results = edits('Sngl', 'SINEGAL')
        assert results == {'removed': ['i', 'e', 'a'], 'changed': [], 'added': []}
        results = edits('Sngl', 'Singl')
        assert results == {'removed': ['i'], 'changed': [], 'added': []}
        results = edits('Sngl', 'Single')
        assert results == {'removed': ['i', 'e'], 'changed': [], 'added': []}
        results = edits('Sngl', 'SINGLEY')
        assert results == {'removed': ['i', 'e', 'y'], 'changed': [], 'added': []}
        results = edits('Sngl', 'SINGLY')
        assert results == {'removed': ['i', 'y'], 'changed': [], 'added': []}
        results = edits('Sngl', 'SNUGLY')
        assert results == {'removed': ['u', 'y'], 'changed': [], 'added': []}
        results = edits('Stanless', 'Stainless')
        assert results == {'removed': ['i'], 'changed': [], 'added': []}
        results = edits('Strght', 'Straight')
        assert results == {'removed': ['a', 'i'], 'changed': [], 'added': []}
        results = edits('Strght', 'STRAUGHT')
        assert results == {'removed': ['a', 'u'], 'changed': [], 'added': []}
        results = edits('Strght', 'STREIGHT')
        assert results == {'removed': ['e', 'i'], 'changed': [], 'added': []}
        results = edits('Strght', 'STRIGHT')
        assert results == {'removed': ['i'], 'changed': [], 'added': []}
        results = edits('Swvl', 'SWAVELY')
        assert results == {'removed': ['a', 'e', 'y'], 'changed': [], 'added': []}
        results = edits('Swvl', 'Swivel')
        assert results == {'removed': ['i', 'e'], 'changed': [], 'added': []}
        results = edits('Telo', 'TELOI')
        assert results == {'removed': ['i'], 'changed': [], 'added': []}
        results = edits('Towabl', 'Towable')
        assert results == {'removed': ['e'], 'changed': [], 'added': []}
        results = edits('Trackw', 'TRACKWAY')
        assert results == {'removed': ['a', 'y'], 'changed': [], 'added': []}
        results = edits('Trav', 'TRAVE')
        assert results == {'removed': ['e'], 'changed': [], 'added': []}
        results = edits('Trilite', 'TROILITE')
        assert results == {'removed': ['o'], 'changed': [], 'added': []}
        results = edits('Twing', 'TAWING')
        assert results == {'removed': ['a'], 'changed': [], 'added': []}
        results = edits('Twing', 'TEWING')
        assert results == {'removed': ['e'], 'changed': [], 'added': []}
        results = edits('Twing', 'Towing')
        assert results == {'removed': ['o'], 'changed': [], 'added': []}
        results = edits('Twing', 'TWINGE')
        assert results == {'removed': ['e'], 'changed': [], 'added': []}
        results = edits('Univers', 'UNIVERSE')
        assert results == {'removed': ['e'], 'changed': [], 'added': []}
        results = edits('Univers', 'universo')
        assert results == {'removed': ['o'], 'changed': [], 'added': []}
        results = edits('Upstd', 'UPSTAYED')
        assert results == {'removed': ['a', 'y', 'e'], 'changed': [], 'added': []}
        results = edits('Upstd', 'UPSTOOD')
        assert results == {'removed': ['o', 'o'], 'changed': [], 'added': []}
        results = edits('Upstnd', 'Upstand')
        assert results == {'removed': ['a'], 'changed': [], 'added': []}
'''
