from itertools import groupby
from collections import Counter, namedtuple
import pyximport; pyximport.install()
from .edit_measurement import *


def Changes_Classification(word1, word2):
    '''Classifies changes transforming word1 to word2.
    If letters are only removed, removed letters are only vowels or consonants are removed only after
    vowels this is classified as abbreviaton
    If changed letters are adjustant on keyboard or sound similar this is typo
    If letter is repeated in word1 less or more times than in word2, this is typo
    Otherwise change is labeled unknown '''

    a_edits = []

    tn_dif = edits(word1, word2)
    word1, word2 = word1.lower(), word2.lower()

    # Check if word1 is singular or plural of word2
    singular_plural = is_sing_plur(word1, word2)
    if singular_plural == 'Singular':
        a_edits.append('Singular')
    elif singular_plural == "Plural":
        a_edits.append('Plural')
    
    elif is_tense(word1, word2):
        a_edits.append('Tense')

    # Check if removed letters mean abbreviaon/truncation/typo
    elif tn_dif.removed and not tn_dif.changed and not tn_dif.added:

            if is_abbreviation(tn_dif, word1, word2):
                a_edits.append('Abbreviation')

            elif is_truncation(word1, word2):
                a_edits.append('Truncation')

            else:
                for letter in tn_dif.removed:
                    if is_typo_add_rem(letter, word1, word2, change='removed'):
                        a_edits.append('Typo')
                    else:
                        a_edits.append('Unknown')

    # Typo checks
    elif tn_dif.removed:
        for letter in tn_dif.removed:
            if is_typo_add_rem(letter, word1, word2, change='removed'):
                a_edits.append('Typo')
            else:
                a_edits.append('Unknown')

    if tn_dif.changed:
        for pair in tn_dif.changed:
            if is_typo_changed(pair):
                a_edits.append('Typo')
            else:
                a_edits.append('Unknown')
                
    if tn_dif.transposed:
        for pair in tn_dif.transposed:
            a_edits.append('Typo')

    if tn_dif.added and is_sing_plur(word1, word2) != 'Plural':
        for letter in tn_dif.added:
            if is_typo_add_rem(letter, word1, word2, change='added'):
                a_edits.append('Typo')
            else:
                a_edits.append('Unknown')

    return sorted(a_edits)


tn_prob = namedtuple("probabilities", [
    "plural", "singular", "tense", "abbreviation", "truncation", "typo", "brand"
])


def Probabilities(word1, word2, tn_edits=None, i_round=None):
    if word1 == word2: 
        return tn_prob(0, 0, 0, 0, 0, 0, 0) 
    
    d_plur, d_sing, d_tense = 0.0, 0.0, 0.0
    d_abbr, d_trunc, d_typo, d_brand = 0.0, 0.0, 0.0, 0.0
    
    if tn_edits is None:
        tn_edits = edits(word1, word2, duplication=True)

    singular_plural = is_sing_plur(word1, word2)
    if singular_plural == 'Singular':
        d_sing = 1.0
    elif singular_plural == 'Plural':
        d_plur = 1.0
    else:
        if is_tense(word1, word2):
            d_tense = 1.0
        else:        
            d_abbr = abbreviation_probability(word1, word2, tn_edits)
            d_trunc = truncation_probability(word1, word2, tn_edits)
            d_typo = typo_probability(word1, word2, tn_edits)
            d_brand = brand_probability(word1, word2, tn_edits)

    if i_round:
        #a_diff = round(a_diff, i_round)
        d_plur, d_sing = round(d_plur, i_round), round(d_sing, i_round)
        d_tense, d_abbr = round(d_tense, i_round), round(d_abbr, i_round)
        d_trunc, d_typo = round(d_trunc, i_round), round(d_typo, i_round)
        d_brand = round(d_brand, i_round)
    
    return tn_prob(
        # different=a_diff,
        plural=d_plur,
        singular=d_sing,
        tense=d_tense,
        abbreviation=d_abbr,
        truncation=d_trunc,
        typo=d_typo,
        brand=d_brand,
)
