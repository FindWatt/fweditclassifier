﻿import math, re
from itertools import groupby
from collections import Counter, namedtuple
from string import punctuation
import numpy as np
from FwText import singularize, pluralize, NLTK_STEMMER

#__all__ = [
#    'tn_edits', 'tn_edits_with_duplication',
#    'edits', 'distance',
#    'is_sing_plur', 'is_typo_add_rem', 'is_abbreviation', 'is_truncation', 'is_typo_changed',
#    'truncate','truncation_probability', 
#    'abbreviation_probability',
#    'typo_probability',
#    'clear_edit_punctuation',
#]

cpdef str s_VOW = 'aeiouyAEIOUY'
cpdef str s_CONS = 'qwrtypsdfghjklzxcvbnmQWRTYPSDFGJJKLZXCVBNM'
cdef set c_TENSES = {"e", "ed", "es", "ing"}

cdef dict e_KEYS = {
    'Q': 'qASW!@12', 'q': 'Aasw12',
    'W': 'wQASDE@#23', 'w': 'Wqasde23',
    'E': 'eWSDFR#$34', 'e': 'Ewsdfr34',
    'R': 'rEDFGT$%45', 'r': 'Redfgt45',
    'T': 'tRFGHY%^56', 't': 'Trfghy56',
    'Y': 'yTGHJU^&67', 'y': 'Ytghju67',
    'U': 'uYHJKI&*78', 'u': 'Uyhjki78',
    'I': 'iUJKLO*(89', 'i': 'Iujklo89',
    'O': 'oIKLP()90', 'o': 'Oikl;p90',
    'P': "pOL)_[';:{0", 'p': "Pol;'[0-",
    'A': 'aZSWQ', 'a': 'Aqwsz',
    'S': 'sWAZXDE', 's': 'Swazxde',
    'D': 'dESXCFR', 'd': 'Desxcfr',
    'F': 'fRDCVGT', 'f': 'Frdcvgt',
    'G': 'gTFVBHY', 'g': 'Gtfvbhy',
    'H': 'hYGBNJU', 'h': 'Hygbnju',
    'J': 'jUHNMKI', 'j': 'Juhnmki',
    'K': 'kIJM<,LO', 'k': 'Kijm,lo',
    'L': 'lOK,.;P:><', 'l': 'Lok,.;p',
    'Z': 'zASX', 'z': 'Zasx',
    'X': 'xZSDC', 'x': 'Xzsdc',
    'C': 'cXDFV', 'c': 'Cxdfv',
    'V': 'vCFGB', 'v': 'Vcfgb',
    'B': 'bVGHN', 'b': 'Bvghn',
    'N': 'nBHJM', 'n': 'Nbhjm',
    'M': 'mNJK,<', 'm': 'Mnjk,'
}

cdef set c_adjacent
for s_letter, s_adjacent in e_KEYS.items():
    c_adjacent = set(s_adjacent)
    e_KEYS[s_letter] = c_adjacent

a_SOUND = ['eahjk', 'bsdgptvz', 'flmnsxz', 'iy', 'quw']
a_SOUND.extend([s_char.upper() for s_char in a_SOUND])
a_CON_PAIRS = ['ck', 'ch', 'sh', 'sch', 'th', 'gh', 'ph']

e_PUNCT = set(punctuation)
tn_edits = namedtuple("edits", ["added", "removed", "changed", "transposed"])
tn_edits_with_duplication = namedtuple("edits", ["added", "removed", "changed", "transposed", "duplicated", "deduplicated"])


cpdef edits(str s_word1, str s_word2, bint duplication=False, bint ignore_punctuation=False, bint ignore_case=True):
    ''' Returns which changes are required to transfrom s_word1 into s_word2 '''
    cdef str s_x, s_y
    cdef int i_rows, i_cols, i_x, i_y, i, j, i_DistHor, i_DistVer, i_DistDiag
    cdef list a_dist, a_align
    cdef list a_added, a_removed, a_changed, a_transposed, a_duplicated, a_deduplicated
    
    if ignore_case:
        s_x, s_y = s_word1.lower(), s_word2.lower()
    else:
        s_x, s_y = s_word1, s_word2
    i_rows, i_cols = len(s_x), len(s_y)
    
    if not s_word1 and not s_word2:
        if duplication:
            return tn_edits_with_duplication([], [], [], [], [], [])
        else:
            return tn_edits([], [], [], [])
    if not s_word1:
        if duplication:
            return tn_edits_with_duplication([], list(s_word2), [], [], [], [])
        else:
            return tn_edits([], list(s_word2), [], [])
    if not s_word2:
        if duplication:
            return tn_edits_with_duplication(list(s_word1), [], [], [], [], [])
        else:
            return tn_edits(list(s_word1), [], [], [])
    
    
    # Creating matrixes a_dist for calulating distanse and a_align to traceback changes
    a_dist = [[0] * (i_cols + 1) for i_x in range(i_rows + 1)]
    a_align = [[0] * (i_cols + 1) for i_x in range(i_rows + 1)]
    a_added, a_removed, a_changed, a_transposed = [], [], [], []
    a_duplicated, a_deduplicated = [], []
    
    # Initializing first column and first row of a_dist
    for i_x in range(1, i_rows + 1): a_dist[i_x][0] = i_x
    for i_y in range(1, i_cols + 1): a_dist[0][i_y] = i_y

    # Filling a_dist with minimal value for distanse from horizontal, vertical, diagonal
    for i_x in range(1, i_rows + 1):
        for i_y in range(1, i_cols + 1):
            i_DistHor = a_dist[i_x][i_y - 1] + 1
            i_DistVer = a_dist[i_x - 1][i_y] + 1
            
            if s_x[i_x - 1] == s_y[i_y - 1]:
                i_DistDiag = a_dist[i_x - 1][i_y - 1]
            else:
                i_DistDiag = a_dist[i_x - 1][i_y - 1] + 1

            a_dist[i_x][i_y] = min(i_DistHor, i_DistVer, i_DistDiag)

            if a_dist[i_x][i_y] == i_DistDiag:
                a_align[i_x][i_y] = 2
            if a_dist[i_x][i_y] == i_DistHor:
                a_align[i_x][i_y] = 1
            if a_dist[i_x][i_y] == i_DistVer:
                a_align[i_x][i_y] = 0

    # Trace back a_align to identify changes that are to be made to transform s_x to s_y
    i, j = len(a_align) - 1, len(a_align[0]) - 1

    while (i > 0 and j > 0):
        i_x, i_y = i - 1, j - 1
        
        if (
            i > 1 and j > 1 and s_y[i_y] != s_x[i_x] and 
            s_x[i_x] == s_y[j - 2] and s_x[i - 2] == s_y[i_y] and
            s_x[i_x] != s_x[i_x - 2] and s_y[i_y] != s_y[i_y - 2]
        ):
            a_transposed.append((s_x[i_x], s_y[i_y]))
            i, j = i - 2, j - 2
        
        elif a_align[i][j] == 0:
            i = i_x
            #print('removed', s_x, s_x[i], i_x, i_y, i, j)
            if duplication and s_x[i] == s_x[i-1]:
                a_duplicated.append(s_x[i])
            else:
                a_added.append(s_x[i])

        elif a_align[i][j] == 1:
            j = i_y
            #print('removed', s_y, s_y[j], i_x, i_y, i, j)
            if duplication and s_y[j] == s_y[j-1]:
                a_deduplicated.append(s_y[j])
            else:
                a_removed.append(s_y[j])
            
        elif a_align[i][j] == 2:

            i, j  = i_x, i_y
            if (s_y[j] != s_x[i]):
                #print('changed', s_y[j], s_x[i], i_x, i_y, i, j)
                a_changed.append((s_x[i], s_y[j]))
                
        else:
            i, j  = i_x, i_y
        
    for i_x in reversed(range(1, i + 1)): a_added.append(s_x[i_x - 1])
    for i_y in reversed(range(1, j + 1)): a_removed.append(s_y[i_y - 1])
    
    a_added = a_added[::-1]
    a_removed = a_removed[::-1]
    a_changed = a_changed[::-1]
    a_transposed = a_transposed[::-1]
    a_duplicated = a_duplicated[::-1]
    a_deduplicated = a_deduplicated[::-1]

    if ignore_punctuation:
        clear_edit_punctuation(a_added, a_removed, a_changed, a_transposed, a_duplicated, a_deduplicated)
    
    if duplication:
        return tn_edits_with_duplication(a_added, a_removed, a_changed, a_transposed, a_duplicated, a_deduplicated)
    else:
        return tn_edits(a_added, a_removed, a_changed, a_transposed)


cpdef int distance(str s_1, str s_2, bint b_case_insensitive=False):
    ''' returns: character damerau-levenshtein distance '''
    cdef int i_len1, i_len2, x, y, i_x, i_y, cost
    cdef str s_x, s_y
    
    if s_1 == s_2: return 0
    i_len1, i_len2 = len(s_1), len(s_2)
    if i_len1 == 0: return i_len2
    if i_len2 == 0: return i_len1
    
    np_dist = np.zeros([i_len1 + 1, i_len2 + 1], dtype=int)
    
    if b_case_insensitive:
        s_1, s_2 = s_1.lower(), s_2.lower()
      
    np_dist[:, 0] = np.array(range(i_len1 + 1), dtype=int)
    np_dist[0, :] = np.array(range(i_len2 + 1), dtype=int)
    
    for i_x, s_x in enumerate(s_1):
        x = i_x + 1
        for i_y, s_y in enumerate(s_2):
            if s_x == s_y:
                cost = 0
            else:
                cost = 1
                
            y = i_y + 1
            np_dist[x, y] = min(
                np_dist[x - 1, y] + 1,  # deletion
                np_dist[x, y - 1] + 1,  # insertion
                np_dist[x - 1, y - 1] + cost  #substitution
            )
            # transposition
            if i_x and i_y and s_x == s_2[i_y - 1] and s_1[i_x - 1] == s_y:
                np_dist[(x, y)] = min (np_dist[(x, y)], np_dist[x-2, y-2] + cost)
                
    return np_dist[-1, -1]


cpdef int sentence_distance(list a_1, list a_2, bint b_case_insensitive=False):
    ''' returns: character damerau-levenshtein distance '''
    cdef int i_len1, i_len2, x, y, i_x, i_y, cost
    cdef str s_x, s_y
    
    if not a_1 or not a_2: return 0
    
    if b_case_insensitive:
        a_1 = [s_x.lower() for s_x in a_1]
        a_2 = [s_y.lower() for s_y in a_2]
    
    if a_1 == a_2: return 0
    i_len1, i_len2 = len(a_1), len(a_2)
    
    np_dist = np.zeros([i_len1 + 1, i_len2 + 1], dtype=int)
      
    np_dist[:, 0] = np.array(range(i_len1 + 1), dtype=int)
    np_dist[0, :] = np.array(range(i_len2 + 1), dtype=int)
    
    for i_x, s_x in enumerate(a_1):
        x = i_x + 1
        for i_y, s_y in enumerate(a_2):
            if s_x == s_y:
                cost = 0
            else:
                cost = 1
                
            y = i_y + 1
            np_dist[x, y] = min(
                np_dist[x - 1, y] + 1,  # deletion
                np_dist[x, y - 1] + 1,  # insertion
                np_dist[x - 1, y - 1] + cost  #substitution
            )
            # transposition
            if i_x and i_y and s_x == a_2[i_y - 1] and a_1[i_x - 1] == s_y:
                np_dist[(x, y)] = min (np_dist[(x, y)], np_dist[x - 2, y - 2] + cost)
                
    return np_dist[-1, -1]


cpdef list group(str word, str query):
    return group_letter(word, query)


cdef list group_letter(str word, str query):
    '''Returns groups of consecutive indexes of query in word '''
    cdef int i, k, i_len
    cdef a_letters, a_indexes, groups
    a_letters = list(word)
    i_len = len(a_letters)
    a_indexes = [i for i in range(i_len)
                 if a_letters[i] == query]
          
    groups = [[item[1] for item in grp]
              for k, grp in groupby(enumerate(a_indexes), lambda x: x[0] - x[1])]
    return groups


cpdef bint removed_from_pair(str letter, str word1, str word2):
    return broken_pair(letter, word1, word2)


cdef bint broken_pair(str letter, str word1, str word2):
    """Checks if a consonant was removed from a pair of consonants that represent one sound"""
    cdef str s_pair, s_let, s_remaining_letter
    a_pairs = [s_pair for s_pair in a_CON_PAIRS if letter in s_pair]

    cdef int k
    for k in range(len(a_pairs)):
        if a_pairs[k] in word2 and a_pairs[k] not in word1:
            s_remaining_letter = [s_let for s_let in a_pairs[k] if s_let != letter][0]
            #print(a_pairs[k], s_remaining_letter)
            if s_remaining_letter in word1:
                return True

    return False


cpdef is_sing_plur(str word1, str word2):
    return is_singular(word1, word2)


cdef is_singular(str word1, str word2):
    """Checks if word1 is singular or plural of word2"""
    cdef int i_len1, i_len2
    i_len1, i_len2 = len(word1), len(word2)
    if i_len1 > i_len2:
        if singularize(word1) == word2:
            return "Plural"
    elif i_len1 < i_len2:
        if pluralize(word1) == word2:
            return "Singular"

        return False


cpdef bint is_tense(str word1, str word2):
    ''' see if words are just different tenses. '''
    cdef str s_stem1, s_stem2, s_suff1, s_suff2
    s_stem1, s_stem2 = NLTK_STEMMER(word1), NLTK_STEMMER(word2)
    cdef int i_stem = len(s_stem1)
    if s_stem1 != s_stem2: return False
    s_suff1, s_suff2 = word1[i_stem:].lower(), word2[i_stem:].lower()
    return (not s_suff1 or s_suff1 in c_TENSES) and (not s_suff2 or s_suff2 in c_TENSES)


#cpdef bint is_typo_add_rem(str letter, str word1, str word2, change='added'):
#    return is_typo(letter, word1, word2, change=change)


cpdef bint is_typo_add_rem(str letter, str word1, str word2, change='added'):
    ''' Checks if removed letter are skipped from duplicate letters in word
        or added letter is an odd repetition '''
    cdef int i

    cdef list a_indexes1 = group(word1, letter)
    cdef list a_indexes2 = group(word2, letter)
    #print('a_indexes1', type(a_indexes1))
    
    if a_indexes1 and a_indexes2:
        for i in range(len(a_indexes1)):
            if change=='added':
                a_indexes1[i].pop(-1)
            elif change=='removed':
                a_indexes1[i].append(a_indexes1[i][-1] + 1)

            if a_indexes1[i] in a_indexes2:
                return True

    return  False


cpdef bint is_abbreviation(tn_dif, str word1, str word2):
        '''Checks if word1 is abbreviation of word2'''
        cdef list a_vow1, a_vow2, a_vow_rem, a_cons_rem
        cdef dict e_cons2
        cdef int i
        cdef str s_char
        
        if word1 == re.sub(r'[aeiouy]', '', word2): return True
        
        #Listing indexes of vowels in word1
        a_vow1 = [i for i, s_char in enumerate(list(word1))
                  if s_char in s_VOW and  word1.index(s_char)!=0 ]

        #Listing indexes of vowels(a_vow2) and consonants(e_cons2) in word1
        a_vow2 = [i for i, s_char in enumerate(list(word2))
                  if s_char in s_VOW]
        e_cons2 = {s_char: i for i, s_char in enumerate(list(word2))
                   if s_char in s_CONS}

        #lstg indexes of removed vowel (a_vow_rem) and removed consonants(a_cons_rem)
        a_vow_rem = [i for i, s_char in enumerate(tn_dif.removed)
                     if s_char in s_VOW]
        a_cons_rem = [s_char for i, s_char in enumerate(tn_dif.removed)
                      if s_char in s_CONS]

        if a_vow_rem and (not a_cons_rem ):
            return True

        elif a_vow_rem and a_cons_rem and not a_vow1:
            if  not [i for i in a_cons_rem if e_cons2[i]<max(a_vow2)]:

               return True

        return False


cpdef bint is_truncation(str word1, str word2):
    return truncation(word1, word2)


cdef bint truncation(str word1, str word2):
    ''' Check if word1 is truncation of word2.
        This applies to words of more than one syllable.
        Looking for two types.
        The first type is where the end of the word is just cut off.
        The second type is where the end is cut off, and then the vowels are removed. '''
    cdef int i
    cdef str s_char, s_check1
    cdef tuple x
    cdef list a_vowels, groups, y
    
    a_vowels = [i for i, s_char in enumerate(list(word2)) if s_char in s_VOW]
    groups = []

    for k, grp in groupby(enumerate(a_vowels), lambda x: x[0] - x[1]):
        groups.append(([(x[1]) for x in grp]))

    if word2[0] in s_VOW:
        if word2[:groups[1][0]] == word1:
            return True
        try:
            if word2[:groups[2][0]]==word1:
                return True
        except IndexError:
            pass
    else:

        try:
            s_check1 = ''.join([s_char for s_char in word2[:groups[1][0]] if s_char not in s_VOW])
            y = [s_char for s_char in word2[groups[0][-1]:groups[1][0]-1] if s_char!='r']
            s_check2 = word2[:groups[0][-1]]+"".join(y)+word2[groups[1][0]-1]
            if word2[:groups[1][0]] == word1 or s_check1 == word1 or s_check2 == word1:
                return True
        except IndexError:
            pass
    return False


cpdef bint is_typo_changed(tuple pair):
    ''' Checks if changed letters in word are adjacent on keyboard or sound similar and so is typo '''
    cdef str i, j
    
    if pair[1] in e_KEYS.get(pair[0], ''):
        return True

    cdef list a_search = [
        [a_SOUND.index(i) for i in a_SOUND if j in i ] for j in pair
    ]
    
    if a_search and len(a_search) == 2 and a_search[0] == a_search[1]:
        return True

    return False


s_TRUNC_VOW = 'aeiouy'
s_TRUNC_CONS = 'qwrtpsdfghjklzxcvbnm'
re_potential_truncate = re.compile(
    r"([{}]*[{}]+)([{}]+)([{}]+)([a-z]*)".format(s_TRUNC_VOW, s_TRUNC_CONS, s_TRUNC_VOW, s_TRUNC_CONS),
    flags=re.I
)

cpdef tuple truncate(str s_word):
    ''' Take a word and return both its truncated versions '''
    cdef tuple t_segs
    cdef str s_tr1, s_tr2
    
    re_word = re_potential_truncate.search(s_word)
    if not re_word or not re_word.group(4): return None
    
    t_segs = re_word.groups()
    s_tr1 = ''.join(t_segs[:3])
    s_tr2 = '{}{}'.format(t_segs[0], t_segs[2])
    
    return s_tr1, s_tr2


cpdef float truncation_probability(str s_word1, str s_word2, tn_edits=None):
    ''' Check if word1 is a truncation of word2.
        Looking for two types.
        The first type is where the end of the word is just cut off.
        The second type is where the end is cut off, and then the vowels are removed.
        This applies to words of more than one syllable.
    '''
    cdef tuple t_trunc
    cdef int i_dist1, i_dist2, i_dist, i_len, i_b_edits
    cdef str s_trunc
    cdef float d_trunc
    
    if not s_word1 or not s_word2: return 0.0
    if (s_word2[0] != s_word1[0] and s_word2[0] in s_CONS): return 0.0
    if not tn_edits:
        tn_edits = edits(s_word1, s_word2, duplication=True)
    
    t_trunc = truncate(s_word2)
    if not t_trunc: return 0.0
    
    if s_word1 == t_trunc[0] or s_word1 == t_trunc[1]: return 1.0
    
    i_dist1 = distance(s_word1, t_trunc[0])
    if i_dist1 > 1:
        i_dist2 = distance(s_word1, t_trunc[1])
    else:
        i_dist2 = 10
    
    if i_dist1 <= i_dist2:
        i_dist = i_dist1
        s_trunc = t_trunc[0]
    else:
        i_dist = i_dist2
        s_trunc = t_trunc[1]
                       
    i_len = max(len(s_word1), len(s_trunc))
    d_trunc = max(float(i_len - i_dist**2) / i_len, 0)
    # print(i_dist, s_trunc, i_len, d_trunc)
    
    i_b_edits = len(tn_edits.added) + len(tn_edits.changed) + len(tn_edits.transposed)
    try:
        i_b_edits += len(tn_edits.duplicated)
    except:
        pass
        
    return d_trunc * (1 / (i_b_edits + 1))


cpdef float abbreviation_probability(str s_word1, str s_word2, tn_edits=None):
    ''' Calculate the probabability that word is an abbreviation '''
    cdef int g_edits, b_edits, m_edits, i
    cdef list a_vow1, a_vow_rem, a_cons_rem, a_cons1
    cdef str s_char
    
    if not s_word1 or not s_word2: return 0.0
    s_word1, s_word2 = s_word1.lower(), s_word2.lower()
    g_edits, b_edits, m_edits = 0, 0, 0
    if not tn_edits:
        tn_edits = edits(s_word1, s_word2, duplication=True)
    
    try:
        a_duped = tn_edits.duplicated
        a_deduped = tn_edits.deduplicated
        b_duplication = True
    except AttributeError:
        a_duped, a_deduped = [], []
        b_duplication = False

    b_edits += len(tn_edits.added) + len(tn_edits.changed) + len(tn_edits.transposed)
    
    # listing:
    a_vow1 = [i for i, s_char in enumerate(list(s_word1))
              if s_char in s_VOW and s_word1.index(s_char) != 0]  # vowels i word 1
    a_vow_rem = [i for i, s_char in enumerate(tn_edits.removed)
                 if s_char in s_VOW]  # removed vovels
    g_edits += len(a_vow_rem)
    
    
    a_cons_rem = [s_char for i, s_char in enumerate(tn_edits.removed)
                  if s_char in s_CONS]  # removed consonants

    if b_duplication:
        g_edits += len(a_deduped)
        #print(g_edits, a_deduped)
        b_edits += len(a_duped)
        
        for s_char in a_cons_rem:
            if removed_from_pair(s_char, s_word1, s_word2):
                g_edits += 1
            else:
                b_edits += 1
        #print(b_edits, a_duped, a_cons_rem)
    else:
        # Check if removed consonants are from duplicate or from pairs that are one sound
        for s_char in a_cons_rem:
            if removed_from_pair(s_char, s_word1, s_word2):
                g_edits += 1
            elif is_typo_add_rem(s_char, s_word1, s_word2, change='removed'):
                g_edits += 1
            else:
                b_edits += 1

    # Counting unremoved vowels in s_word1 as missed edits
    if a_vow1: m_edits += len(a_vow1)

    # Check if unremoved consonants are duplicates
#    a_cons1 = [s_char for s_char in s_word1 if s_char in s_CONS]
#    for s_char in a_cons1:
#        for k in (group(s_word1, s_char)):
#            if len(k) > 1:
#                #print('k', k)
#                m_edits += 1

    # Check if unremoved consonants are single sound pairs
    for s_char in a_CON_PAIRS:
        if s_char in s_word1:
            #print('s_con_pair', s_char)
            m_edits += 1
    
    # print(g_edits, m_edits, b_edits)
    if g_edits == 0: return 0.0
    
    cdef float d_abbr = (float(g_edits - b_edits) / float(g_edits + b_edits + m_edits))**2
    
    if s_word1[0] == s_word2[0]:
        return max(d_abbr, 0.0)
    else:  # give extra weight for change to the first character since that is less likely to be a typo
        return max(d_abbr * 0.5, 0.0)


cdef int i_good_log_depth = 3
cdef int i_bad_log_depth = 4
cpdef float typo_probability(str s_word1, str s_word2, tn_edits=None):
    cdef int g_edits, b_edits
    cdef str s_w1, s_w2
    cdef list a_dupes, a_dedupes
    cdef float d_good, d_bad
    
    g_edits, b_edits = 0, 0
    if not s_word1 or not s_word2: return 0.0
    s_w1, s_w2 = s_word1.lower(), s_word2.lower()
    if s_w1 == s_w2: return 0.0
    
    if not tn_edits:
        tn_edits = edits(s_word1, s_word2, duplication=True)
    
    try:
        a_dupes = tn_edits.duplicated
    except AttributeError:
        a_dupes = []
        
    try:
        a_dedupes = tn_edits.deduplicated
    except AttributeError:
        a_dedupes = []
        
    for letter in tn_edits.added:
        #print(letter, letter.isupper(), s_word2.istitle(), s_word2.islower())
        if is_typo_add_rem(letter, s_w1, s_w2, change='added'):
            g_edits += 1
        elif letter.isupper() and (s_word2.istitle() or s_word2.islower()):
            b_edits += 2
        else:
            b_edits += 1
            
    for letter in tn_edits.removed:
        if is_typo_add_rem(letter, s_w1, s_w2, change='removed'):
            g_edits += 1
        else:
            b_edits += 1
                      
    for pair in tn_edits.changed:
        if pair[0].lower() == pair[1].lower():
            pass
        elif is_typo_changed(pair):
            g_edits += 1
        else:
            b_edits += 1

    for pair in tn_edits.transposed:
        g_edits += 1
        
    for letter in a_dupes:
        g_edits += 1
        
    for letter in a_dedupes:
        g_edits += 1
    
    if g_edits + b_edits == 0:
        return 0.0
    
    d_good = max(1 - math.log(max(g_edits, 1), i_good_log_depth), 0)
    d_bad = max(1 - math.log(b_edits + 1, i_bad_log_depth), 0)
    
    if s_w1[0] == s_w2[0]:
        return d_good * d_bad
    else:  # give extra weight for change to the first character since that is less likely to be a typo
        return d_good * d_bad * 0.5


# re patterns for brand edits
re_start_letter = re.compile(r'^[A-Zx]-?(.+)')
re_end_letter = re.compile(r'(.+)-?[A-Zx]$')
re_start_pro = re.compile(r'^pro-?(.+)', flags=re.I)
re_end_pro = re.compile(r'(.+)-?pro$', flags=re.I)
re_lite = re.compile(r'ight')  #light -> lite, night -> nite
re_ck = re.compile(r'ck')  #quick -> quik, pack -> pak
re_ch = re.compile(r'ch')  #tech -> tek
re_low = re.compile(r'low')  #low -> lo
re_ph = re.compile(r'ph')  #photo -> foto
re_y_ie = re.compile(r'y$')  #binky -> binkie
re_i_y = re.compile(r'y')  #binky -> binkie


cpdef brand_probability(s_word1, s_word2, tn_edits=None):
    ''' calculate the probability that word1 is a brand/product-name version of word2
        common brand patterns are:
            change end 's' to 'z'
            change end 'ze' to 'zz'
            removed 'e' from end
            added 'y' to end
            doubled 'z', 'v'
            change 'ight' -> 'ite'
            change 'low' -> 'lo'
            change 'quick' -> 'quik'
            change 'tech' -> 'tek'
            change 'photo' -> 'foto'
            add 1 capital letter at end or start (optional dash)
            add 'pro' at end or start (optional dash)
            is camel case
            ' n '
            '-a-'
    '''
    cdef str s_w1, s_w2
    cdef bint b_added, b_removed, b_changed, b_duplicated, b_deduplicated
    cdef list a_ind, a_duped
    
    #if not s_word1: return 0.0
    if not s_word1 or not s_word2: return 0.0
    
    if not tn_edits:
        tn_edits = edits(s_word1, s_word2, duplication=True)
    
    #print(tn_edits)
    if tn_edits.transposed:
        #print('transposed', tn_edits)
        return 0.0
    s_w1, s_w2 = s_word1.lower(), s_word2.lower()
    b_added = True if tn_edits.added else False
    b_removed = True if tn_edits.removed else False
    b_changed = True if tn_edits.changed else False
    
    
    try:
        b_duplicated = True if tn_edits.duplicated else False
        b_deduplicated = True if tn_edits.deduplicated else False
        a_duped = tn_edits.duplicated
        #a_deduped = tn_edits.deduplicated
    except AttributeError:
        b_duplicated, b_deduplicated = False, False
        a_duped = []
        #a_deduped = []
        
    # look for end 's' turned into 'z'
    if (s_w1[-1] == 'z' and s_w2[-1] == 's' and
        not b_added and not b_removed
    ):
        return 1.0
        
    # look for end 'ze' turned into 'zz'
    if (s_w1[-2:] == 'zz' and s_w2[-2:] == 'ze' and
        not b_added and not b_removed
    ):
        return 1.0
    
    # changed 'c' to 'k'
    if b_changed and not b_removed and not b_added and not b_duplicated and not b_deduplicated:
        if len(tn_edits.changed) == 1:
            if tn_edits.changed[0][0] in 'cC' and tn_edits.changed[0][1] in 'kK':
                return 0.5
            if tn_edits.changed[0][0] in 'iI' and tn_edits.changed[0][1] in 'yY':
                return 0.75
            
    # removed end 'e'
    if b_removed and not b_changed and not b_added and not b_duplicated and not b_deduplicated:
        if len(tn_edits.removed) == 1 and tn_edits.removed[0] in 'eE':
            if s_word1 == s_word2[:-1]:
                return 0.5
            
    # added end 'y' or 'r'
    if b_added and not b_changed and not b_removed and not b_duplicated and not b_deduplicated:
        if len(tn_edits.added) == 1 and tn_edits.added[0] in 'yYrR':
            if s_word1[:-1] == s_word2:
                return 0.75
    
    # doubled v, x, or z
    if b_duplicated and not b_deduplicated and not b_added and not b_changed and not b_removed:
        if len(a_duped) == 1 and a_duped[0] in 'vVxXzZ':
            return 0.75
    
    if b_added and not b_changed and not b_removed and not b_duplicated and not b_deduplicated:
        # look for doubled 'zz, vv
        if len(tn_edits.added) == 1 and tn_edits.added[0] in 'zZvVxX':
            if s_word1[:-1] == s_word2:
                return 0.75
            a_ind = group(s_w1, 'z')
            if any([len(pair) > 1 for pair in a_ind]):
                return 0.75
            
            a_ind = group(s_w1, 'v')
            if any([len(pair) > 1 for pair in a_ind]):
                return 0.75
            
            a_ind = group(s_w1, 'x')
            if any([len(pair) > 1 for pair in a_ind]):
                return 0.75
    
        # look for a single added capital letter at the start of the word
        o_start = re_start_letter.search(s_word1)
        if o_start and o_start.group(1) == s_word2:
            return 1.0

        # look for a single added capital letter at the end of the word
        o_end = re_end_letter.search(s_word1)
        if o_end and o_end.group(1) == s_word2:
            return 1.0
    
        # look for added 'pro' at the start of the word
        o_start = re_start_pro.search(s_word1)
        if o_start and o_start.group(1) == s_word2:
            return 1.0

        # look for added 'pro' at the end of the word
        o_end = re_end_pro.search(s_word1)
        if o_end and o_end.group(1) == s_word2:
            return 1.0

        
    # look for light -> lite type changes
    if re_lite.sub('ite', s_w2) == s_w1:
        return 1.0
    
    # look for quick -> quik type changes
    if re_ck.sub('k', s_w2) == s_w1:
        return 0.75
    elif re_ck.sub('c', s_w2) == s_w1:
        return 0.75
    
    # look for tech -> tek type changes
    if re_ch.sub('k', s_w2) == s_w1:
        return 0.75
    
    # look for low -> lo type changes
    if re_low.sub('lo', s_w2) == s_w1:
        return 0.75
    
    # look for photo -> foto type changes
    if re_ph.sub('f', s_w2) == s_w1:
        return 0.75
    
    # look for binky -> binkie type changes
    if re_y_ie.sub('ie', s_w2) == s_w1:
        return 0.75
    
    return 0.0


cpdef clear_edit_punctuation(list a_added, list a_removed, list a_changed,list a_transposed,
                             list a_duplicated=[], list a_deduplicated=[]):
    ''' Remove punctuation characters from edit lists.
        This happens in place, so no need to return it. '''
    cdef str s_char
    cdef tuple t_change
    cdef bint b_char1_punct, b_char2_punct
    
    for s_char in reversed(a_removed):
        if s_char in e_PUNCT:
            a_removed.remove(s_char)
    
    for s_char in reversed(a_added):
        if s_char in e_PUNCT:
            a_added.remove(s_char)
    
    for t_change in reversed(a_changed):
        b_char1_punct = t_change[0] in e_PUNCT
        b_char2_punct = t_change[1] in e_PUNCT
        
        if not b_char1_punct and not b_char2_punct:
            pass
        
        elif b_char1_punct and not b_char2_punct:
            a_removed.append(t_change[1])
            a_changed.remove(t_change)
    
        elif not b_char1_punct and b_char2_punct:
            a_added.append(t_change[0])
            a_changed.remove(t_change)
            
        elif b_char1_punct and b_char2_punct:
            a_changed.remove(t_change)
    
    for t_change in reversed(a_transposed):
        b_char1_punct = t_change[0] in e_PUNCT
        b_char2_punct = t_change[1] in e_PUNCT

        if b_char1_punct or b_char2_punct:
            a_transposed.remove(t_change)
    
    for s_char in reversed(a_duplicated):
        if s_char in e_PUNCT:
            a_duplicated.remove(s_char)
    
    for s_char in reversed(a_deduplicated):
        if s_char in e_PUNCT:
            a_deduplicated.remove(s_char)

cpdef only_punctuation_differences(tn_edits):
    ''' Check if edits are only punctuation. '''
    cdef str s_char
    cdef tuple t_change
    for s_char in tn_edits.added + tn_edits.removed:
        if not s_char in e_PUNCT:
            return False
        
    for t_change in tn_edits.changed + tn_edits.transposed:
        if t_change[0] not in e_PUNCT or t_change[1] not in e_PUNCT:
            return False
    
    try:
        for s_char in tn_edits.duplicated:
            if s_char not in e_PUNCT:
                return False
    except:
        pass
        
    try:
        for s_char in tn_edits.deduplicated:
            if s_char not in e_PUNCT:
                return False
    except:
        pass
        
    return True
